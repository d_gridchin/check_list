<?php

class index extends Engine_Content {


    public function process() {
        // Нужно получить все бланки
        $blank = new Blank();
        $blank->addWhere('add_date', date('Y-m-').'01', '>=');
        $blank->addWhere('add_date', date('Y-m-t'), '<=');

        $blankArray = [];


        while ($x = $blank->getNext()) {
            $shop = Shops_Service::Get()->getShopById($x->getShop_id());
            if ($shop['deleted']) {
                continue;
            }
            $blankArray[] = [
                'id' => $x->getId(),
                'shop_number' => $shop['number'],
                'shop' => $shop,
                'auditor' => Auth_Service::Get()->getUserById($x->getAuditor_id()),
                'rm_id' => $shop['rm']['id']
            ];
        }

        $rm_id = array();
        $shopNumber = [];
        foreach ($blankArray as $k => $v) {
            $rm_id[$k] = $v['rm_id'];
            $shopNumber[$k] = $v['shop_number'];
        }

        if (Engine::Get()->getConfigField('check_list_type') == 'auditor') {
            array_multisort($shopNumber, SORT_ASC, $blankArray);
        }
        if (Engine::Get()->getConfigField('check_list_type') == 'inventory') {

        }



        $this->setValue('blankArray', $blankArray);

    }

}