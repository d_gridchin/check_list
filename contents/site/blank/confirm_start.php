<?php

class confirm_start extends Engine_Content {

    public function process() {
        $user = Auth_Service::Get()->getUser();
        $id = $this->getArgument('blank_hash');
        $this->setValue('id', $id);

        if ($user) {
            $blank = new Blank();
            $blank->addWhere('md5(`id`)', $id);
            if ($blank = $blank->getNext()) {
                $shop = Shops_Service::Get()->getShopById($blank->getShop_id());
                if ($shop) {
                    $this->setValue('shop_number', $shop['number']);
                    $this->setValue('shop_name', $shop['city'].', '.$shop['address']);
                }

                if ($this->getArgument('ok')) {
                    $blank->setAuditor_id($user->getId());
                    $blank->update();

                    Main_Service::Get()->addAdditionalInfoToBlank($blank, 'sellers', $this->getArgument('sellers'));

                    header('location: ' . Engine_URL_Maker::Get()->makeUrlByContentId('blank-auditor', ['hash' => md5($blank->getId())]));
                }
            }
        }
    }

}