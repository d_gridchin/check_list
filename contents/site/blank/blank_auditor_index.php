<?php

class blank_auditor_index extends Engine_Content {

    public function __construct() {

    }

    public function process() {
        // Нужно получить все вопросы, которые прикреплены к этому бланку
        $blank = new Blank();
        $blank->addWhere("md5(`id`)", $this->getArgument('hash'));
        if ($blank = $blank->getNext()) {
            $this->setControlValue('id', $blank->getId());
            $this->setValue('shop', Shops_Service::Get()->getShopById($blank->getShop_id()));
            $this->setValue('add_date', $blank->getAdd_date());


            // Получаем текущий вопрос
            $questionNumber = $this->getArgument('question_number');
            $customRate = $this->getArgument('custom_rate');
            $this->setValue('custom_rate', $customRate);

            $submitForm = false;
            if ($questionNumber && ($this->getArgument('ok_yes') || $this->getArgument('ok_no') || $this->getArgument('ok_individually') || $this->getArgument('ok_rate') !== false)) {
                $submitForm = true;
                $bq = new BlankQuestion();
                $bq->setId($questionNumber);
                if ($bq->select()) {
                    // Получаем сам вопрос
                    $tmpQuestion = new Questions();
                    $tmpQuestion->setId($bq->getQuestion_id());
                    if ($tmpQuestion->select()) {
                        if ($this->getArgument('ok_yes')) {
                            $bq->setRate($tmpQuestion->getRate());
                            $bq->setIndividual(0);
                        }
                        if ($this->getArgument('ok_no')) {
                            $bq->setRate(0);
                            $bq->setIndividual(0);
                        }
                        if ($this->getArgument('ok_individually')) {
                            $bq->setIndividual(1);
                        }

                        if ($this->getArgument('ok_rate') !== false) {
                            $bq->setRate($this->getArgument('ok_rate'));
                            $bq->setIndividual(0);
                        }

                    }
                    $bq->setNote($this->getArgument('note'));
                    $bq->setProgress_auditor(1);

                    $bq->setAuditor_complete_date(date('Y-m-d H:i:s'));
                    $bq->update();

                    if ($returnId = $this->getArgument('return')) {
                        header('location: '.Engine_URL_Maker::Get()->makeUrlByContentId('admin-blank-view', ['id' => $returnId]).'?recalculate='.$this->getArgument('recalculate'));
                        exit();
                    }
                }
            }

            $header = false;


            // Все вопросы
            $bq = new BlankQuestion();
            $bq->setBlank_id($blank->getId());

            $allComplete = true;

            $allQuestionsArray = [];
            while ($x = $bq->getNext()) {
                $q = new Questions();
                $q->setId($x->getQuestion_id());
                if ($q->select()) {
                    if (!$questionNumber && !$x->getProgress_auditor()) {
                        $questionNumber = $x->getId();
                        $header = true;
                    }

                     // Переходим на сл.вопрос
                     if ($submitForm && $questionNumber == $x->getId()) {
                        $submitForm = false;
                        $questionNumber = false;
                     }

                    $a = [
                        'id' => $x->getId(),
                        'question' => $q->getName(),
                        'type' => $q->getType(),
                        'max_rate' => $q->getRate(),
                        'rate' => $x->getRate(),
                        'progress' => $x->getProgress_auditor(),
                        'note' => $x->getNote(),
                        'active' => $questionNumber == $x->getId(),
                        'category_id' => $q->getCategory_id()
                    ];

                    if (!$x->getProgress_auditor()) {
                        $allComplete = false;
                    }

                    if ($questionNumber == $x->getId()) {
                        $this->setValue('currentQuestion', $a);
                    }
                    if (!isset($allQuestionsArray[$q->getCategory_id()]) || !$allQuestionsArray[$q->getCategory_id()]) {
                        $allQuestionsArray[$q->getCategory_id()] = [];
                    }
                    $allQuestionsArray[$q->getCategory_id()][] = $a;
                }

            }

            // Показываем фотки
            $image = new Images();
            $image->addWhereArray([$blank->getId(), $questionNumber], 'parent_id');

            $imageArray = [];
            $imageBlankArray = [];
            while ($i = $image->getNext()) {
                $a = ['path' => $i->getPath(), 'id' => $i->getId()];
                if ($i->getType() == 'question') {
                    $imageArray[] = $a;
                }

                if ($i->getType() == 'blank') {
                    $imageBlankArray[] = $a;
                }
            }
            $this->setValue('imageArray', $imageArray);
            $this->setValue('imageBlankArray', $imageBlankArray);


            // Переходим на первый вопрос
            $this->setValue('questionNumber', $questionNumber);
            if ($header && $questionNumber) {
                // var_dump($questionNumber);
                header('location: '.Engine_URL_Maker::Get()->makeUrlByContentId('blank-auditor', ['hash' => $this->getArgument('hash'), 'question_number' => $questionNumber]));
            }

            if ($allComplete) {
                if (!$blank->getComplete_date() || $blank->getComplete_date() == '0000-00-00 00:00:00') {
                    $blank->setComplete_date(date('Y-m-d H:i:s'));
                    $blank->update();
                }
            }
            $this->setValue('allComplete', $allComplete);
            $this->setValue('allQuestionsArray', $allQuestionsArray);
            $this->setValue('allCategoryArray', Questions_Service::Get()->getCategoryArray(true));

        } else {
            // todo go to 404
        }
    }

}