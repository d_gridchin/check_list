<?php

class site_template extends Engine_Content {

    public function process() {
        if (!Auth_Service::Get()->isLogin()) {
            $this->setContentId('admin_auth');
        }
        $user = Auth_Service::Get()->getUser();
        $this->setValue('role', $user->getRole());
        $this->setValue('access_admin', in_array($user->getRole(), ['admin', 'tm', 'rm']));
    }

}