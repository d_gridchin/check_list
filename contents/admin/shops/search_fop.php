<?php

class search_fop extends Engine_Content {

    public function __construct() {

    }

    public function process() {
        $query = mb_strtolower($this->getArgument('name'));

        $fop = new XFop();
        $fop->addWhereQuery('LOWER(`name`) LIKE "%'.$query.'%"');
        $fop->setLimit(0, 5);
        $return = [];
        while ($x = $fop->getNext()) {
            $return[] = $x->getName();
        }

        print json_encode($return);
        exit();
    }

}