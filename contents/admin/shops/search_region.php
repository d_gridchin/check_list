<?php

class search_region extends Engine_Content {

    public function process() {
        $query = mb_strtolower($this->getArgument('name'));

        $region = new XRegion();
        $region->addWhereQuery('LOWER(`name`) LIKE "%'.$query.'%"');
        $region->setLimit(0, 5);
        $return = [];
        while ($x = $region->getNext()) {
            $return[] = $x->getName();
        }

        print json_encode($return);
        exit();
    }

}