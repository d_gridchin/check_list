<?php

class shops_control extends Engine_Content {


    public function process() {
        $id = $this->getArgument('id');
        $type = $this->getArgument('type');



        $resultArray = [];

        if ($type == 'get_info') {
            $resultArray['type_list'] = Engine::Get()->getConfigField('check_list_type');
            if ($id) {
                $shop = new XShops();
                $shop->setId($id);
                $controlTmId = false;
                $controlRmId = false;
                $controlPresidentId = false;
                if ($shop->select()) {
                    $resultArray['number'] = $shop->getNumber();
                    $resultArray['address'] = $shop->getAddress();
                    $resultArray['fop'] = Shops_Service::Get()->getFopById($shop->getFop_id());
                    $resultArray['city'] = Shops_Service::Get()->getCityById($shop->getCity_id());
                    $resultArray['id'] = $shop->getId();

                    $resultArray['region'] = Shops_Service::Get()->getRegionById($shop->getRegion_id());

                    $controlTmId = $shop->getTm_user_id();
                    $controlRmId = $shop->getRm_user_id();
                    $controlPresidentId = $shop->getPresident_id();

                    $resultArray['status'] = 'ok';

                } else {
                    $resultArray['status'] = 'question-not-found';
                }

                $usersArray = [];
                foreach (['tm', 'rm', 'president'] as $role) {
                    $usersArray[$role] = [];
                    $usersArray[$role][] = ['name' => '--- Выберите ---', 'value' => 0];
                }

                $users = new Users();
                $users->setDeleted(0);
                $users->setOrder('name', 'ASC');
                while ($x = $users->getNext()) {
                    $selected = false;
                    if ($x->getRole() == 'tm' && $x->getId() == $controlTmId) $selected = true;
                    if ($x->getRole() == 'rm' && $x->getId() == $controlRmId) $selected = true;
                    if ($x->getRole() == 'president' && $x->getId() == $controlPresidentId) $selected = true;
                    $usersArray[$x->getRole()][] = [
                        'name' => $x->getName(),
                        'value' => $x->getId(),
                        'selected' => $selected
                    ];
                }
                $resultArray['tm_array'] = $usersArray['tm'];
                $resultArray['rm_array'] = $usersArray['rm'];
                $resultArray['president_array'] = $usersArray['president'];
            } else {
                $resultArray['status'] = 'add-new';
            }
        }

        if ($type == 'delete'){
            $shop = new XShops();
            $shop->setId($id);
            if ($shop->select()) {
                $shop->setDeleted(1);
                $shop->update();

                $resultArray['status'] = 'deleted';
            } else {
                $resultArray['status'] = 'question-not-found';
            }
        }

        if ($type == 'control') {
//            print "<pre>";
//            print_r($this->getArguments());
//            print "</pre>";

            $number = $this->getArgument('number');
            $address = $this->getArgument('address');
            $city = $this->getArgument('city');
            $fop = $this->getArgument('fop');

            $region = $this->getArgument('region');


            $c = new XCity();
            $c->setName($city);
            if (!$c->select()) {
                $c->insert();
            }

            if ($fop) {
                $f = new XFop();
                $f->setName($fop);
                if (!$f->select()) {
                    $f->insert();
                }
            }


            if ($region) {
                $r = new XRegion();
                $r->setName($region);
                if (!$r->select()) {
                    $r->insert();
                }
            }



            $shop = new XShops();
            if ($id == 'add') {
                $shop->insert();
            } else {
                $shop->setId($id);
                if (!$shop->select()) {
                    $resultArray['status'] = 'shop-not-found';
                }
            }


            if ($this->getArgument('tm_id')) {
                $shop->setTm_user_id($this->getArgument('tm_id'));
            }

            if ($this->getArgument('rm_id')) {
                $shop->setRm_user_id($this->getArgument('rm_id'));
            }

            if ($this->getArgument('president_id')) {
                $shop->setPresident_id($this->getArgument('president_id'));
            }


            $shop->setNumber($number);
            $shop->setAddress($address);

            $shop->setCity_id($c->getId());

            if (isset($f)) {
                $shop->setFop_id($f->getId());
            }
            if (isset($r)) {
                $shop->setRegion_id($r->getId());
            }
            $shop->update();

            $resultArray['status'] = 'ok';
        }

        echo json_encode($resultArray);
        exit();
    }

}