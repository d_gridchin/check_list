<?php

class search_city extends Engine_Content {

    public function __construct() {

    }

    public function process() {
        $query = mb_strtolower($this->getArgument('name'));

        $city = new XCity();
        $city->addWhereQuery('LOWER(`name`) LIKE "%'.$query.'%"');
        $city->setLimit(0, 5);
        $return = [];
        while ($x = $city->getNext()) {
            $return[] = $x->getName();
        }

        print json_encode($return);
        exit();
    }

}