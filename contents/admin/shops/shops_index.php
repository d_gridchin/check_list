<?php

class shops_index extends Engine_Content {

    public function __construct() {

    }

    public function process() {
        // Все города
        $filter_city_id = $this->getArgument('filter_city_id');
        $cityArray = [];
        $city = new XCity();
        $city->setOrder('name', 'ASC');
        while ($x = $city->getNext()) {
            $cityArray[] = ['id' => $x->getId(), 'name' => $x->getName(), 'selected' => $x->getId() == $filter_city_id];
        }
        $this->setValue('cityArray', $cityArray);

        // Все пользователи (ТМ / РМ / Ревизору)
        $filter_auditor_id = $this->getArgument('filter_auditor_id');

        $filter_rm_id = $this->getArgument('filter_rm_id');
        $filter_tm_id = $this->getArgument('filter_tm_id');
        $filter_president = $this->getArgument('filter_president');

        $filter_auditor_complete = $this->getArgument('filter_auditor_complete');
        $filter_complete_comments = $this->getArgument('filter_complete_comments');

        $this->setValue('filter_auditor_complete', $filter_auditor_complete);
        $this->setValue('filter_complete_comments', $filter_complete_comments);

        $auditorArray = [];
        $rmArray = [];
        $tmArray = [];
        $presidentArray = [];

        $user = new Users();
        $user->setDeleted(0);
        while ($x = $user->getNext()) {
            $a = ['id' => $x->getId(), 'name' => $x->getName()];
            if ($x->getRole() == 'auditor') {
                $a['selected'] = $x->getId() == $filter_auditor_id;
                $auditorArray[] = $a;
            }
            if ($x->getRole() == 'rm') {
                $a['selected'] = $x->getId() == $filter_rm_id;
                $rmArray[] = $a;
            }
            if ($x->getRole() == 'tm') {
                $a['selected'] = $x->getId() == $filter_tm_id;
                $tmArray[] = $a;
            }
            if ($x->getRole() == 'president') {
                $a['selected'] = $x->getId() == $filter_president;
                $presidentArray[] = $a;
            }

        }
        $this->setValue('auditorArray', $auditorArray);
        $this->setValue('rmArray', $rmArray);
        $this->setValue('tmArray', $tmArray);
        $this->setValue('presidentArray', $presidentArray);

        $fopArray = [];
        $fop = new XFop();
        while ($x = $fop->getNext()) {
            $fopArray[] = ['id' => $x->getId(), 'name' => $x->getName(), 'selected' => $x->getId() == $this->getArgument('filter_fop_id')];
        }
        $this->setValue('fopArray', $fopArray);

        // Получаем все регионы
        $regionArray = [];
        $region = new XRegion();
        while ($x = $region->getNext()) {
            $regionArray[] = ['id' => $x->getId(), 'name' => $x->getName(), 'selected' => $x->getId() == $this->getArgument('filter_region')];
        }

        $this->setValue('region_array', $regionArray);


        $this->setValue('shopsArray', Shops_Service::Get()->getList());
    }

}