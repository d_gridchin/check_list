<?php

class logout extends Engine_Content {


    public function process() {
        Auth_Service::Get()->logOut();
        header('location: /admin/');
    }

}