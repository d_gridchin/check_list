<?php

class auth extends Engine_Content {

    public function __construct() {

    }

    public function process() {
        Engine_HTMLHead::Get()->registerCssFile('/_css/login.css', 'end_of_list');

        if ($this->getArgument('auth')) {
            $login = Auth_Service::Get()->login($this->getArgument('login'), $this->getArgument('password'));
            if ($login) {
                header('location: .');
            } else {
                $this->setValue('error', true);
            }
        }
    }

}