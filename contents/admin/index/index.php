<?php

class index extends Engine_Content {


    public function process() {
        $date = $this->getArgument('date');
        if (!$date) {
            $date = date('Y-m-d');
        }
        $this->setValue('date', $date);
        $this->setValue('nextUrl', Engine_URL_Maker::Get()->makeUrlByContentIdGet('admin-index', [], ['date' => date('Y-m-d', strtotime($date.' +1 month'))]));
        $this->setValue('prevUrl', Engine_URL_Maker::Get()->makeUrlByContentIdGet('admin-index', [], ['date' => date('Y-m-d', strtotime($date.' -1 month'))]));

    }

}