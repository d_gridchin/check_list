<?php

class index_ajax_load extends Engine_Content {

    public function __construct() {

    }

    public function process() {
        $date = $this->getArgument('date');
        if (!$date) {
            $date = date('Y-m-d');
        }

        $blank = new Blank();
        $blank->addWhere('add_date', date('Y-m-', strtotime($date)).'01', '>=');
        $blank->addWhere('add_date', date('Y-m-t', strtotime($date)), '<=');




        $completeCnt = ['yes' => 0, 'no' => 0];
        $rmArray = [];
        $presidentArray = [];
        $categoryArray = [];
        $regionArray = [];
        $shopPercent = [
            ['name' => 'Лучшие', 'min' => 98, 'max' => 100, 'cnt' => 0, 'color' => '#6cf900'],
            ['name' => 'Средние', 'min' => 96, 'max' => 97, 'cnt' => 0, 'color' => '#cbf900'],
            ['name' => 'Ниже среднего', 'min' => 94, 'max' => 95, 'cnt' => 0, 'color' => '#f9f000'],
            ['name' => 'Больше минимума', 'min' => 91, 'max' => 93, 'cnt' => 0, 'color' => '#f9c300'],
            ['name' => 'Худшие', 'min' => 1, 'max' => 90, 'cnt' => 0, 'color' => '#f90000'],
        ];

        $top = [];

        while ($x = $blank->getNext()) {
            // Заполнена анкета, или нет
            $complete = Questions_Service::Get()->checkBlankComplete($x->getId());

            // Получаем ТМ
            $shop = Shops_Service::Get()->getShopById($x->getShop_id());
            $top[$x->getShop_id()] = [
                'name' => $shop['name'],
                'address' => $shop['address'],
                'city' => $shop['city'],
                'number' => $shop['number'],
                'rate' => 0
            ];

            if (isset($shop['rm']) && $shop['rm']) {
                if (!isset($rmArray[$shop['rm']['id']]) || !$rmArray[$shop['rm']['id']]) {
                    $rmArray[$shop['rm']['id']] = [
                        'name' => $shop['rm']['name'],
                        'all_rate' => 0,
                        'all_cnt' => 0
                    ];
                }
            }
            if (isset($shop['president']) && $shop['president']) {
                if (!isset($presidentArray[$shop['president']['id']]) || !$presidentArray[$shop['president']['id']]) {
                    $presidentArray[$shop['president']['id']] = [
                        'name' => $shop['president']['name'],
                        'all_rate' => 0,
                        'all_cnt' => 0
                    ];
                }
            }

            $rate = 0;
            $cnt = 0;

            if ($x->getRate()) {
                $rate = $x->getRate();
            } else {
                $blankQuestion = new BlankQuestion();
                $blankQuestion->setBlank_id($x->getId());
                while ($bq = $blankQuestion->getNext()) {
                    $q = Questions_Service::Get()->getQuestionById($bq->getQuestion_id());

                    if ($complete) {
                        if (!isset($categoryArray[$q['category_id']]) || !$categoryArray[$q['category_id']]) {
                            $category = Questions_Service::Get()->getCategoryById($q['category_id']);
                            $categoryArray[$q['category_id']] = [
                                'id' => $category['id'],
                                'name' => $category['name'],
                                'items' => [

                                ]
                            ];

                        }
                        if (!isset($regionArray[$shop['region_id']]) || !$regionArray[$shop['region_id']]) {
                            $regionArray[$shop['region_id']] = [
                                'id' => $shop['region_id'],
                                'name' => $shop['region'],
                                'items' => [

                                ]
                            ];

                        }
                    }



                    if (!$bq->getIndividual() && $complete) {
                        $rate += (($bq->getRate() / $q['rate']) * 100);
                        $cnt++;
                        if ($shop['rm']) {
                            $rmArray[$shop['rm']['id']]['all_rate'] += (($bq->getRate() / $q['rate']) * 100);
                            $rmArray[$shop['rm']['id']]['all_cnt'] ++;

                            $categoryArray[$q['category_id']]['items'][$shop['rm']['id']]['name'] = $shop['rm']['name'];
                            $categoryArray[$q['category_id']]['items'][$shop['rm']['id']]['all_rate'] += (($bq->getRate() / $q['rate']) * 100);
                            $categoryArray[$q['category_id']]['items'][$shop['rm']['id']]['all_cnt'] ++;
                        }

                        if ($shop['president']) {
                            $presidentArray[$shop['president']['id']]['all_rate'] += (($bq->getRate() / $q['rate']) * 100);
                            $presidentArray[$shop['president']['id']]['all_cnt'] ++;

                            $regionArray[$shop['region_id']]['items'][$shop['president']['id']]['name'] = $shop['president']['name'];
                            $regionArray[$shop['region_id']]['items'][$shop['president']['id']]['all_rate'] += (($bq->getRate() / $q['rate']) * 100);
                            $regionArray[$shop['region_id']]['items'][$shop['president']['id']]['all_cnt']++;
                        }
                    }
                }

                $rate = $rate / $cnt;
            }

            if (Engine::Get()->getConfigField('check_list_type') == 'auditor') {
                $rate = floor($rate);
            }


            foreach ($shopPercent as &$s) {
                if ($rate >= $s['min'] && $rate <= $s['max']) {
                    $s['cnt']++;
                }
            }

            $top[$x->getShop_id()]['rate'] = $rate;


            // Если анкета заполнена, нужно посчитать оценку


            // Отмечаем заполнена, или нет
            if ($complete) {
                $completeCnt['yes']++;
            } else {
                $completeCnt['no']++;
            }
        }

        $topHigh = $top;
        $topLow = $top;


        uasort($topHigh, function($a, $b ){
            return ($a['rate'] < $b['rate']);
        });
        uasort($topLow, function($a, $b ){
            return ($a['rate'] > $b['rate']);
        });
        
        $this->setValue('topHigh', $topHigh);
        $this->setValue('topLow', $topLow);



        $this->setValue('rm_array', $rmArray);
        $this->setValue('president_array', $presidentArray);
        $this->setValue('completeCnt', $completeCnt);
        $this->setValue('categoryArray', $categoryArray);
        $this->setValue('regionArray', $regionArray);
        $this->setValue('shopPercent', $shopPercent);
        $this->setValue('top', $top);
    }



}