<?php

class users_index extends Engine_Content {

    public function process() {
        $id = $this->getArgument('id');
        $this->setControlValue('id', $id);


        if ($this->getArgument('ok')) {
            $name = $this->getArgument('name');
            $login = $this->getArgument('login');
            $role = $this->getArgument('role');
            $password = $this->getArgument('password');

            $errorArray = [];
            if (!$name) $errorArray[] = 'name';
            if (!$login) $errorArray[] = 'login';
            if (!$role) $errorArray[] = 'role';
            if ($id == 'add' && !$password) $errorArray[] = 'password';

            if ($errorArray) {
                $this->setValue('errorArray', $errorArray);
            } else {
                $user = new Users();
                if ($id == 'add') {
                    $user->insert();
                } else {
                    $user->setId($id);
                    if (!$user->select()) {
                        // TODO go to 404 page
                    }
                }
                $user->setName($name);
                $user->setLogin($login);
                $user->setRole($role);
                if ($password) {
                    $user->setPassword(Auth_Service::Get()->createPasswordHash($password));
                }
                $user->update();
            }
        }

        if ($this->getArgument('delete')) {
            $user = new Users();
            $user->setId($id);
            if ($user->select()) {
                $user->setDeleted(1);
                $user->update();

                header('location: '.Engine_URL_Maker::Get()->makeUrlByContentId('admin-users'));
                exit();
            }
        }

        if ($id && $id != 'add') {
            $user = new Users();
            $user->setId($id);
            if ($user->select()) {
                foreach ($user->getFields() as $f) {
                    $this->setControlValue($f, $user->getField($f));
                }
            }
        }

        // Получаем список всех пользователей
        $this->setValue('usersArray', Auth_Service::Get()->getUserArray());

    }

}