<?php

class control extends Engine_Content {


    public function process() {
        $id = $this->getArgument('id');
        $type = $this->getArgument('type');

        $resultArray = [];

        if ($type == 'get_info') {
            $controlCategoryId = false;
            if ($id) {
                $question = new Questions();
                $question->setId($id);
                if ($question->select()) {
                    $resultArray['name'] = $question->getName();
                    $resultArray['type'] = $question->getType();
                    $resultArray['rate'] = $question->getRate();
                    $resultArray['deleted'] = $question->getType();

                    $controlCategoryId = $question->getCategory_id();

                    $resultArray['status'] = 'ok';

                } else {
                    $resultArray['status'] = 'question-not-found';
                }
            } else {
                $resultArray['status'] = 'add-new';
            }


            $categoryArray = [];
            $categoryArray[] = ['name' => '--- Выберите категорию ---', 'value' => '0'];
            foreach (Questions_Service::Get()->getCategoryArray() as $i => $v) {
                $categoryArray[] = ['name' => $v['name'], 'value' => $v['id'], 'selected' => $controlCategoryId == $v['id']];
            }
            $categoryArray[] = ['name' => 'Добавить категорию', 'value' => 'add-new'];
            $resultArray['category_array'] = $categoryArray;
            $resultArray['type_array'] = Questions_Service::Get()->getTypeArray();
            if (Engine::Get()->getConfigField('check_list_type') == 'inventory') {
                $resultArray['show_category'] = false;
            } else {
                $resultArray['show_category'] = true;
            }
        }

        if ($type == 'delete') {
            if ($id) {
                $question = new Questions();
                $question->setId($id);
                if ($question->select()) {
                    $question->setDeleted(1);
                    $question->update();

                    $resultArray['status'] = 'deleted';
                } else {
                    $resultArray['status'] = 'question-not-found';
                }
            }
        }

        if ($type == 'delete_category') {
            if ($id) {
                $category = new XQuestionCategory();
                $category->setId($id);
                if ($category->select()) {
                    $category->setDeleted(1);
                    $category->update();

                    $questions = new Questions();
                    $questions->setCategory_id($id);
                    while ($x = $questions->getNext()) {
                        $x->setDeleted(1);
                        $x->update();
                    }

                    $resultArray['status'] = 'deleted';
                } else {
                    $resultArray['status'] = 'question-not-found';
                }
            }
        }

        if ($type == 'control') {

            $name = $this->getArgument('name');
            $rate = $this->getArgument('rate');
            $systemType = $this->getArgument('system_type');
            if (!$systemType) {
                $systemType = 'yes_no';
            }
            $deleted = $this->getArgument('deleted');

            $question = new Questions();
            if ($id == 'add') {
                $question->insert();
            } else {
                $question->setId($id);
                if (!$question->select()) {
                    $resultArray['status'] = 'question-not-found';
                }
            }
            $categoryId = $this->getArgument('category_id');
            if ($categoryId == 'add-new') {
                $cat = new XQuestionCategory();
                $cat->setName($this->getArgument('category'));
                if (!$cat->select()) {
                    $categoryId = $cat->insert();
                }
            }

            $question->setCategory_id($categoryId);
            $question->setName($name);
            $question->setType($systemType);
            $question->setRate($rate);
            $question->setDeleted($deleted);

            $question->update();

            $resultArray['status'] = 'ok';
        }

        echo json_encode($resultArray);
        exit();
    }

}