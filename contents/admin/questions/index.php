<?php

class index extends Engine_Content {

    public function process() {
        $questionArray = [];
        foreach (Questions_Service::Get()->getAllQuestionsArray() as $i => $v) {
            if (!isset($questionArray[$v['category_id']])) {
                $questionArray[$v['category_id']] = [];
            }
            $questionArray[$v['category_id']][] = $v;
        }
        $this->setValue('categoryArray', Questions_Service::Get()->getCategoryArray());
        $this->setValue('questionsArray', $questionArray);
    }

}