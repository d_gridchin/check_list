<?php

class index_ajax_load_data extends Engine_Content {

    public function process() {
        $date = $this->getArgument('date');
        $this->setValue('blankArray', Questions_Service::Get()->getReportBlankAll($date));
    }

}