<?php

class index extends Engine_Content {

    public function process() {
        $date = $this->getArgument('date');
        if (!$date) {
            $date = date('Y-m-d');
        }
        $this->setValue('date', $date);
        $chartRate = $this->getArgument('char_rate');
        $chartRate = preg_replace("/[^0-9\-]/", '', $chartRate);;
        $this->setValue('char_rate', $chartRate);

        $this->setValue('nextUrl', Engine_URL_Maker::Get()->makeUrlByContentIdGet('admin-blanks', [], ['date' => date('Y-m-d', strtotime($date.' +1 month'))]));
        $this->setValue('prevUrl', Engine_URL_Maker::Get()->makeUrlByContentIdGet('admin-blanks', [], ['date' => date('Y-m-d', strtotime($date.' -1 month'))]));

        PackageLoader::Get()->loadHelper('bootstrap_select');

        // Все города
        $filter_city_id = $this->getArgument('filter_city_id');
        $cityArray = [];
        $city = new XCity();
        $city->setOrder('name', 'ASC');
        while ($x = $city->getNext()) {
            $cityArray[] = ['id' => $x->getId(), 'name' => $x->getName(), 'selected' => $x->getId() == $filter_city_id];
        }
        $this->setValue('cityArray', $cityArray);

        // Получаем все регионы
        $filter_region_id = $this->getArgument('filter_region_id');
        $regionArray = [];
        $region = new XRegion();
        while ($x = $region->getNext()) {
            $regionArray[] = ['id' => $x->getId(), 'name' => $x->getName(), 'selected' => $x->getId() == $filter_region_id];
        }

        $this->setValue('regionArray', $regionArray);

        // Все пользователи (ТМ / РМ / Ревизору)
        $filter_auditor_id = $this->getArgument('filter_auditor_id');
        $filter_rm_id = $this->getArgument('filter_rm_id');
        $filter_tm_id = $this->getArgument('filter_tm_id');
        $filter_president_id = $this->getArgument('filter_president_id');
        $filter_auditor_complete = $this->getArgument('filter_auditor_complete');
        $filter_complete_comments = $this->getArgument('filter_complete_comments');

        $this->setValue('filter_auditor_complete', $filter_auditor_complete);
        $this->setValue('filter_complete_comments', $filter_complete_comments);

        $this->setValue('control_filter_tm_id', $this->getArgument('filter_tm_id'));

        $auditorArray = [];
        $rmArray = [];
        $tmArray = [];
        $presidentArray = [];
        $user = new Users();
        $user->setDeleted(0);
        while ($x = $user->getNext()) {
            $a = ['id' => $x->getId(), 'name' => $x->getName()];
            if ($x->getRole() == 'auditor') {
                $a['selected'] = $x->getId() == $filter_auditor_id;
                $auditorArray[] = $a;
            }
            if ($x->getRole() == 'rm') {
                $a['selected'] = $x->getId() == $filter_rm_id;
                $rmArray[] = $a;
            }
            if ($x->getRole() == 'tm') {
                $a['selected'] = $x->getId() == $filter_tm_id;
                $tmArray[] = $a;
            }
            if ($x->getRole() == 'president') {
                $a['selected'] = $x->getId() == $filter_president_id;
                $presidentArray[] = $a;
            }
        }
        $this->setValue('auditorArray', $auditorArray);
        $this->setValue('rmArray', $rmArray);
        $this->setValue('tmArray', $tmArray);
        $this->setValue('presidentArray', $presidentArray);

    }

}