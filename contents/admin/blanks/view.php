<?php

class view extends Engine_Content {

    public function process() {
        PackageLoader::Get()->loadHelper('XLSXWriter');
        PackageLoader::Get()->loadHelper('XEditable');
        $user = Auth_Service::Get()->getUser();
        $this->setValue('user_role', $user->getRole());


        $id = $this->getArgument('id');
        $this->setControlValue('id', $id);

        if ($this->getArgument('update_comment')) {
            $blankQuestion = new BlankQuestion();
            $blankQuestion->setId($this->getArgument('change_comment_id'));
            if ($blankQuestion->select()) {
                $blankQuestion->setComment($this->getArgument('comment'));
                $blankQuestion->update();
            }

        }


        $blank = new Blank();
        $blank->setId($id);
        if ($blank = $blank->getNext()) {
            $shop = Shops_Service::Get()->getShopById($blank->getShop_id());
            $recalculate = $this->getArgument('recalculate');
            if ($this->getArgument('edit_rate')) {
                $blank->setRate($this->getArgument('value'));
                $blank->setCustomRate(1);
                $blank->update();

                $recalculate = false;
            }
            if ($recalculate) {
                $blank->setCustomRate(0);
                $blank->setRate(0);
                $blank->update();

                header('location: '.Engine_URL_Maker::Get()->makeUrlByContentId('admin-blank-view', ['id' => $id]));
                exit();
            }
            $allRate = $blank->getRate();


            $completeDate = '?';
            if ($blank->getComplete_date() && $blank->getComplete_date() != '0000-00-00 00:00:00') {
                $completeDate = $blank->getComplete_date();
            }
            $this->setValue('complete_date', $completeDate);
            $this->setValue('id', $blank->getId());
            $this->setValue('shop', $shop);
            $this->setValue('auditor', Auth_Service::Get()->getUserById($blank->getAuditor_id()));
            $this->setValue('add_date', $blank->getAdd_date());

            // Поиск по вопросам
            $search_question = $this->getArgument('search_question');
            if (!$search_question) {
                $search_question = '';
            }
            $this->setControlValue('search_question', $search_question);

            $bq = new BlankQuestion();
            $bq->setBlank_id($blank->getId());



            $questionsArray = [];

            $calculate = false;
            if (!$allRate && !$blank->getCustomRate()) {
                $calculate = true;
            }
            $qc = 0;

            while ($x = $bq->getNext()) {
                $q = new Questions();
                $q->setId($x->getQuestion_id());
                if ($q->select()) {
                    if (!isset($questionsArray[$q->getCategory_id()]) || !$questionsArray[$q->getCategory_id()]) {
                        $questionsArray[$q->getCategory_id()] = [];
                    }

                    $imagesArray = [];
                    $image = new Images();
                    $image->setType('question');
                    $image->setParent_id($x->getId());
                    while ($i = $image->getNext()) {
                        if (strpos($i->getPath(), Main_Service::Get()->getSiteAddress()) === false) {
                            $path = Main_Service::Get()->getSiteAddress().$i->getPath();
                        } else {
                            $path = $i->getPath();
                        }
                        $imagesArray[] = ['path' => $path, 'id' => $i->getId()];
                    }

                    $hide = false;
//                    if (strpos(mb_strtolower($q->getName()), mb_strtolower($search_question)) === false && $search_question) {
//                        $hide = true;
//                    }
                    $questionsArray[$q->getCategory_id()][] = [
                        'id' => $x->getId(),
                        'name' => $q->getName(),
                        'rate' => $x->getRate(),
                        'individual' => $x->getIndividual(),
                        'note' => $x->getNote(),
                        'max_rate' => $q->getRate(),
                        'comment' => $x->getComment(),
                        'images' => $imagesArray,
                        'hide' => $hide
                    ];
                }
            }
            if ($calculate) {
                $info = Questions_Service::Get()->getBlankInfo($id, true);
                if ($info) {
                    $allRate = $info['rate'] / $info['cnt'];
                }
            }
            $allCategory = Questions_Service::Get()->getCategoryArray(true);
            $this->setValue('categoryArray', Questions_Service::Get()->getCategoryArray(true));
            $this->setValue('questionsArray', $questionsArray);


            // Экспорт в XLSX
            if ($this->getArgument('export') && $this->getArgument('export') == 'xlsx') {

                $writer = new XLSXWriter();
                foreach($questionsArray as $categoryId => $question) {
                    $writer->writeSheetRow('Sheet1', ['#', $allCategory[$categoryId]['name'], 'Оценка', 'Примечание', 'Комментарий ТМ'], ['fill' => '#eeeeee', 'height' => '14', 'halign' => 'center', 'font-size'=>14, 'border'=>'left,right,top,bottom', 'font-style'=>'bold']);
                    foreach ($question as $q) {
                        $rate = 'Инд.';
                        if (!$q['individual']) {
                            $rate = number_format(($q['rate'] / $q['max_rate'] * 100), 2, '.', ' ').'%';
                        }
                        $a = [
                            $q['id'],
                            $q['name'],
                            $rate,
                            $q['note'],
                            $q['comment'],
                        ];
                        $writer->writeSheetRow('Sheet1', $a);
                    }
                    $writer->writeSheetRow('Sheet1', []);

                }
                // $writer->writeToFile(Engine::Get()->getProjectPath().'/media/xlsx-simple.xlsx');
                $filename = "export_".time().".xlsx";
                header('Content-disposition: attachment;   filename="'.XLSXWriter::sanitize_filename($filename).'"');
                header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                header('Content-Transfer-Encoding: binary');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                $writer->writeToStdOut();

                header("location: ".Engine_URL_Maker::Get()->makeUrlByContentIdGet('admin-blank-view', ['id' => $id]));
                exit();
            }

            // Дополнительные изображения
            $otherImageArray = [];
            $image = new Images();
            $image->setType('blank');
            $image->setParent_id($blank->getId());
            while ($i = $image->getNext()) {
                if (strpos($i->getPath(), Main_Service::Get()->getSiteAddress()) === false) {
                    $path = Main_Service::Get()->getSiteAddress().$i->getPath();
                } else {
                    $path = $i->getPath();
                }
                $otherImageArray[] = ['path' => $path, 'id' => $i->getId()];
                // $otherImageArray[] = ['path' => Main_Service::Get()->getSiteAddress().$i->getPath(), 'id' => $i->getId()];
            }
            $this->setValue('otherImageArray', $otherImageArray);

            if (Engine::Get()->getConfigField('check_list_type') == 'auditor') {
                $allRate = floor($allRate);
            }
            if (!$blank->getCustomRate()) {
                // $blank->setRate($allRate);
                // $blank->update();
            }
            $this->setValue('customRate', $blank->getCustomRate());
            $this->setValue('rate', $allRate);
        } else {
            // TODO Go to 404 page
        }
    }

}