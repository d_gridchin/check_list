<?php

class create_new extends Engine_Content {

    public function process() {
        $name = $this->getArgument('name');
        $blank = new XBlank();
        $blank->setShop_name($name);
        $blank->setAdd_date(date('Y-m-d'));
        $blank->insert();

        $allCnt = 0;

        foreach (Questions_Service::Get()->getAllQuestionsArray() as $q) {
            $bq = new XBlankQuestion();
            $bq->setBlank_id($blank->getId());
            $bq->setQuestion_id($q['id']);
            $bq->insert();

            $allCnt++;
        }
        $blank->setAll_cnt($allCnt);
        $blank->update();

        echo json_encode(['url' => 'http://'.$_SERVER['HTTP_HOST'].Engine_URL_Maker::Get()->makeUrlByContentId('blank', ['hash' => md5($blank->getId())])]);
    }

}