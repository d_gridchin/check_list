<?php

class add_photo_comments extends Engine_Content {

    public function process() {
        $photoId = $this->getArgument('photo_id');
        $text = $this->getArgument('text');

        $user = Auth_Service::Get()->getUser();


        if ($user) {
            $comment = new ImageComments();
            $comment->setImage_id($photoId);
            $comment->setComment($text);
            $comment->setDatetime(date('Y-m-d H:i:s'));
            $comment->setUser_ud($user->getId());
            $comment->insert();

            print 'ok';
        } else {
            print 'fail';
        }


    }

}