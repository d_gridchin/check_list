<?php

class get_photo_comments extends Engine_Content {


    public function process() {
        $commentsArray = [];
        $id = $this->getArgument('id');

        $comments = new ImageComments();
        $comments->setImage_id($id);
        while ($x = $comments->getNext()) {
            $userName = '';
            $user = new Users();
            $user->setId($x->getUser_ud());
            if ($user->select()) {
                $userName = $user->getName();
            }
            $commentsArray[] = [
                'date' => $x->getDatetime(),
                'text' => $x->getComment(),
                'user_name' => $userName

            ];
        }

        echo json_encode($commentsArray);
    }

}