<?php

class index_smart extends Engine_Content {


    public function process() {
        $date = $this->getArgument('date');
        if (!$date) {
            $date = date('Y-m-d');
        }
        $this->setValue('date', $date);
        $this->setValue('nextUrl', Engine_URL_Maker::Get()->makeUrlByContentIdGet('admin-smart-report', [], ['date' => date('Y-m-d', strtotime($date.' +1 month'))]));
        $this->setValue('prevUrl', Engine_URL_Maker::Get()->makeUrlByContentIdGet('admin-smart-report', [], ['date' => date('Y-m-d', strtotime($date.' -1 month'))]));

        $step = $this->getArgument('step');
        if (!$step) {
            $step = 1;
        }
        $this->setValue('step', $step);


        foreach ($this->getArguments() as $k => $v) {
            $this->setControlValue($k, $v);
        }

        $steps = [];
        $steps[] = ['name' => 'Что показывать?', 'active' => ($step == 1)];
        $steps[] = ['name' => 'Уточнения', 'active' => ($step == 2)];
        $steps[] = ['name' => 'Условия', 'active' => ($step == 3)];

        $this->setValue('steps', $steps);



        if ($step == 4) {
            // Начинаем накапливать массив
            $resultArray = [];
            if ($this->getArgument('step_1_value') == 'questions' || $this->getArgument('step_1_value') == 'blank' || $this->getArgument('step_1_value') == 'regions') {
                $blanks = new Blank();
                $blanks->addWhere('add_date', date('Y-m-', strtotime($date)).'01', '>=');
                $blanks->addWhere('add_date', date('Y-m-t', strtotime($date)), '<=');

                while ($blank = $blanks->getNext()) {
                    $complete = Questions_Service::Get()->checkBlankComplete($blank->getId());


                    $continue = false;
                    if ($this->getArgument('step_1_value') == 'blank') {
                        if ($this->getArgument('step_2_value') == 'all') {
                            $complete = 1;
                        }

                        if ($this->getArgument('step_2_value') == 'complete') {
                            if (!$complete) $continue = true;
                        }

                        if ($this->getArgument('step_2_value') == 'no_complete') {
                            if ($complete) $continue = true;
                        }
                    }

                    if ($this->getArgument('step_1_value') == 'regions') {
                        if ($this->getArgument('step_2_value') == 'all') {
                            $complete = 1;
                        }

                        if ($this->getArgument('step_2_value') == 'complete') {
                            if (!$complete) $continue = true;
                        }
                    }

                    if ($continue) continue;

                    if ($complete) {
                        $bq = new BlankQuestion();
                        $bq->setBlank_id($blank->getId());


                        if ($this->getArgument('step_1_value') == 'questions') {
                            if ($this->getArgument('step_2_value') != 'all') {
                                $bq->setQuestion_id($this->getArgument('step_2_value'));
                            }
                        }



                        while ($x = $bq->getNext()) {
                            $q = Questions_Service::Get()->getQuestionById($x->getQuestion_id());
                            if ($this->getArgument('step_1_value') == 'questions') {
                                if (!isset($resultArray[$x->getQuestion_id()]) || !$resultArray[$x->getQuestion_id()]) {
                                    $resultArray[$x->getQuestion_id()] = [
                                        'name' => $q['name'],
                                        'rate' => 0,
                                        'rate_cnt' => 0
                                    ];
                                }

                                $resultArray[$x->getQuestion_id()]['rate'] += (float) ((($x->getRate() / $q['rate']) * 100));
                                $resultArray[$x->getQuestion_id()]['rate_cnt']++;
                            }

                            $shop = Shops_Service::Get()->getShopById($blank->getShop_id());
                            if ($this->getArgument('step_1_value') == 'blank') {
                                if (!isset($resultArray[$blank->getShop_id()]) || !$resultArray[$blank->getShop_id()]) {
                                    $resultArray[$blank->getShop_id()] = [
                                        'name' => '#'.$shop['number'].', '.$shop['city'].', '.$shop['city'],
                                        'rate' => 0,
                                        'rate_cnt' => 0
                                    ];
                                }

                                $resultArray[$blank->getShop_id()]['rate'] += (float) ((($x->getRate() / $q['rate']) * 100));
                                $resultArray[$blank->getShop_id()]['rate_cnt']++;
                            }

                            if ($this->getArgument('step_1_value') == 'regions') {
                                if (!isset($resultArray[$shop['region_id']]) || !$resultArray[$shop['region_id']]) {
                                    $resultArray[$shop['region_id']] = [
                                        'name' => $shop['region'],
                                        'rate' => 0,
                                        'rate_cnt' => 0
                                    ];
                                }

                                $resultArray[$shop['region_id']]['rate'] += (float) ((($x->getRate() / $q['rate']) * 100));
                                $resultArray[$shop['region_id']]['rate_cnt']++;
                            }
                        }
                    }

                }
            }

            foreach ($resultArray as $k => &$v) {
                $allRate = ($v['rate'] / $v['rate_cnt']);
                $v['total_rate'] = $allRate;

                $add = true;
                if ($this->getArgument('step_3_value') == 'rate_big_80') {
                    if ($allRate < 80) {
                        $add = false;
                    }
                }
                if ($this->getArgument('step_3_value') == 'rate_min_80') {
                    if ($allRate >= 80) {
                        $add = false;
                    }
                }

                if (!$add) {
                    unset($resultArray[$k]);
                }

            }
            $this->setValue('resultArray', $resultArray);

        }









    }

}