<?php

class tpl_admin extends Engine_Content {

    public function process() {
        Engine_HTMLHead::Get()->registerCssFile('/_css/admin.css', 'end_of_list');
        $user = Auth_Service::Get()->getUser();

        $adminMenuArray = [];
        $adminMenuArray[] = ['name' => 'Сводная информация', 'url' => Engine_URL_Maker::Get()->makeUrlByContentId('admin-index'), 'active' => in_array($this->getContentId(), ['admin-index'])];

        $adminMenuArray[] = ['name' => 'Анкеты', 'url' => Engine_URL_Maker::Get()->makeUrlByContentId('admin-blanks'), 'active' => in_array($this->getContentId(), ['admin-blanks', 'admin-blank-view'])];

        if ($user->getRole() == 'admin') {
            $adminMenuArray[] = ['name' => 'Все магазины', 'url' => Engine_URL_Maker::Get()->makeUrlByContentId('admin-shops'), 'active' => in_array($this->getContentId(), ['admin-shops'])];
            $adminMenuArray[] = ['name' => 'Все вопросы для анкеты', 'url' => Engine_URL_Maker::Get()->makeUrlByContentId('admin-questions'), 'active' => in_array($this->getContentId(), ['admin-questions'])];
            $adminMenuArray[] = ['name' => 'Пользователи системы', 'url' => Engine_URL_Maker::Get()->makeUrlByContentId('admin-users'), 'active' => in_array($this->getContentId(), ['admin-users'])];

            if (Engine::Get()->getConfigField('check_list_type') == 'inventory') {
                $adminMenuArray[] = ['name' => 'SMART REPORT', 'url' => Engine_URL_Maker::Get()->makeUrlByContentId('admin-smart-report'), 'active' => in_array($this->getContentId(), ['admin-smart-report'])];
            }
        }
        $this->setValue('menuArray', $adminMenuArray);
    }

}