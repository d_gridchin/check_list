<?php

class delete_once_image extends Engine_Content {


    public function process() {
        $image = new Images();
        $image->setId($this->getArgument('id'));
        if ($image->select()) {
            unlink(Engine::Get()->getProjectPath().$image->getPath());
            $image->delete();

        }
    }

}