<?php

class html_template extends Engine_Content {

    public function process() {
        Engine_HTMLHead::Get()->registerJsFile('/_js/loading.js');
        Engine_HTMLHead::Get()->registerJsFile('/_js/creator.form.js');
        Engine_HTMLHead::Get()->registerJsFile('/_js/standard_modal.js');
        Engine_HTMLHead::Get()->registerJsFile('/_js/typeahead.js');
        Engine_HTMLHead::Get()->registerJsFile('/_js/main.js');

        Engine_HTMLHead::Get()->registerCssFile('/_css/site.css', false);

        Engine_HTMLHead::Get()->registerCssFile('/_fonts/awesome/font-awesome.min.css');
        $this->setValue('htmlHead', Engine_HTMLHead::Get()->makeHtmlHead());
    }

}