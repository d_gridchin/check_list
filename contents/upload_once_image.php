<?php

class upload_once_image extends Engine_Content {

    public function process() {
        $image = $this->getArgument('upload_once_file');
        $section = $this->getArgument('section');

        $blankQuestionId = $this->getArgument('blank_question_id');
        $blankId = $this->getArgument('blank_id');


        $id = ($section == 'question') ? $blankQuestionId : $blankId;
        if ($image && is_array($image) && isset($image['tmp_name']) && $image['tmp_name']) {
            $uploadUrl = Image_Service::Get()->makeUploadPath($image['name'], $image['tmp_name']);
            copy($image['tmp_name'], Engine::Get()->getProjectPath().''.$uploadUrl);

            $image = new Images();
            $image->setType($this->getArgument('section'));
            $image->setPath(Main_Service::Get()->getSiteAddress().$uploadUrl);
            $image->setParent_id($id);
            $image->insert();


            echo json_encode(['path' => $uploadUrl, 'id' => $image->getId()]);
        }
        exit();

    }

}