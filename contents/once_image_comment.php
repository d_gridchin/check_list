<?php

class once_image_comment extends Engine_Content {


    public function process() {
        $comment = new Images();
        $comment->setId($this->getArgument('id'));
        if ($comment->select()) {
            if ($this->getArgument('type') == 'get_comment') {
                echo $comment->getComment();
            }
            if ($this->getArgument('type') == 'update') {
                $comment->setComment($this->getArgument('comment'));
                $comment->update();
            }
        }

        exit();

    }

}