<?php

/**
 * Class DB
 */
class DB {

    private $_connect;
    public function connect ($host, $baseName, $user, $password, $driver = 'mysql') {
        try {
            $this->_connect = new PDO("{$driver}:host={$host};dbname={$baseName}", $user, $password);
            $this->_connect->exec('set names utf8');
            return $this->_connect;
        } catch (Exception $e) {
            print "Не удалось подключиться к базе данных. ";
            print "<pre>{$e->getMessage()}</pre>";
        }
    }

    /**
     * @param $tableName
     * @return bool
     */
    public function tableExists($tableName) {
        try {
            $result = DB::Get()->getConnect()->query("SELECT 1 FROM {$tableName} LIMIT 1");
        } catch (Exception $e) {
            return false;
        }

        return $result !== false;
    }

    public function query ($query) {
        return DB::Get()->getConnect()->query($query);
    }

    /**
     * @return PDO
     */
    public function getConnect () {
        return $this->_connect;
    }

    public static function Get() {
        if (!self::$_Instance) {
            self::$_Instance = new self();
        }
        return self::$_Instance;
    }

    private static $_Instance = null;
}