<?php

$dir = str_replace(Engine::Get()->getProjectPath(), '', dirname(__FILE__));
Engine_HTMLHead::Get()->registerJsFile($dir.'/js/bootstrap.min.js');
Engine_HTMLHead::Get()->registerCssFile($dir.'/css/bootstrap.min.css');
Engine_HTMLHead::Get()->registerCssFile($dir.'/css/bootstrap-theme.min.css');