<?php

class Observer {

    private $_eventsArray = array();
    private $_eventsMethodArray = array();
    private $_eventsParamsArray = array();
    public function addEvent ($eventName, $class, $methodName = 'notify', $params = array()) {
        $this->_eventsArray[$eventName][] = $class;
        $this->_eventsMethodArray[$eventName] = array(get_class($class) => $methodName);
    }

    public function observe ($eventName, $params = array()) {
        if (isset($this->_eventsArray[$eventName])) {
            foreach ($this->_eventsArray[$eventName] as $class) {
                $className = get_class($class);
                $methodName = $this->_eventsMethodArray[$eventName][$className];

                $class->$methodName($params);
            }
        } else {
            if (Engine::Get()->getMode('log')) {
                throw new Exception('Event '.$eventName.' do not registered. ');
            }
        }
    }



    public static function Get() {
        if (!self::$_Instance) {
            self::$_Instance = new self();
        }
        return self::$_Instance;
    }

    private static $_Instance = null;
}