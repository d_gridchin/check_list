<?php

spl_autoload_register ('autoLoad');
function autoLoad ($className) {
    foreach (PackageLoader::Get()->getPhpDirectory() as $register) {
        $fileName = Engine::Get()->getProjectPath().$register['directoryName'].$className.$register['extension'];
        if (file_exists($fileName)) {
            include_once ("{$fileName}");
            break;
        }
    }
}