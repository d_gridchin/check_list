<?php

/**
 * Class PackageLoader
 */
class PackageLoader {

    private function preLoad () {
        if (Engine::Get()->getMode('development')) {
            PackageLoader::Get()->loadPackage('VersionControl');
            // $url = 'https://mysvn.ru/redlab/templates/avers/';

            // $svn = new VersionControl_SVN();
            // $options = array('fetchmode' => VERSIONCONTROL_SVN_FETCHMODE_ARRAY);
            // $svn = VersionControl_SVN::factory('Checkout', $options);

            // Define any switches and aguments we may need
            // $switches = array('username' => 'dima', 'password' => 'redlabadmin');
            // $args = array($url, Engine::Get()->getProjectPath().'/'.Engine_Content_Driver::Get()->getBaseFolder().'/test/');

            // Run command
//            if ($output = $svn->run($args, $switches)) {
//                // print_r($output);
//            }
        }


    }

    private $_loadPackagesArray = array();
    public function loadPackage ($packageName) {
        $this->_loadPackagesArray[] = $packageName;
        include_once (Engine::Get()->getProjectPath().'/packages/'.$packageName.'/include.php');
    }

    private $_loadHelpersArray = array();
    public function loadHelper ($helperName) {
        $this->_loadHelpersArray[] = $helperName;
        include_once (Engine::Get()->getProjectPath().'/_api/helpers/'.$helperName.'/include.php');
    }

    private $_phpDirectory = array();
    public function registerPhpDirectory ($directoryName, $extension = '.class.php') {
        $this->_phpDirectory[] = array('directoryName' => $directoryName, 'extension' => $extension);
    }

    public function getPhpDirectory () {
        return $this->_phpDirectory;
    }

    public static function Get() {
        if (!self::$_Instance) {
            self::$_Instance = new self();
        }
        return self::$_Instance;
    }

    private static $_Instance = null;
}