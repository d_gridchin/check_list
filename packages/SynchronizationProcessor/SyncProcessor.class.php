<?php

/**
 * Class SyncProcessor
 */
class SyncProcessor {

    private $_tablesArray = array();
    private $_classesNameArray = array();
    public function syncTableFields ($tableName, $fields, $className = false) {
        // $this->_tablesArray[$tableName]['id'] = 'INT(11) AUTO_INCREMENT PRIMARY KEY';
        foreach ($fields as $k => $v) {
            $this->_classesNameArray[$tableName] = $className;
            $this->_tablesArray[$tableName][$v[0]] = $v[1];
        }
    }

    public function syncTableFieldsProcess () {
        foreach ($this->_tablesArray as $tableName => $columns) {
            if (!DB::Get()->tableExists($tableName)) {
                DB::Get()->getConnect()->query("CREATE TABLE {$tableName} (id int(11) NOT NULL AUTO_INCREMENT, PRIMARY KEY (id))");
            }

            // Получаем все поля
            $f = DB::Get()->getConnect()->prepare("SELECT * FROM {$tableName}");
            $f->execute();
            $x = $f->fetch(PDO::FETCH_ASSOC);
            foreach ($columns as $columnName => $type) {
                if (!$x || !isset($x[$columnName])) {
                    $r = DB::Get()->getConnect()->prepare("ALTER TABLE {$tableName} ADD {$columnName} {$type} NOT NULL");
                } else {
                    $r = DB::Get()->getConnect()->prepare("ALTER TABLE {$tableName} CHANGE {$columnName} {$columnName} {$type} NOT NULL");
                }
                $r->execute();
            }

            if (!$this->_classesNameArray[$tableName]) {
                continue;
            }

            // генерируем файл с классом

            $columns['id'] = 'int(11)';


            $fdata = '<?php'."\n";
            $fdata .= "/**\n";
            $fdata .= " * Class {$this->_classesNameArray[$tableName]}\n";
            $fdata .= " */\n";
            $fdata .= "class {$this->_classesNameArray[$tableName]} extends DBObject {\n\n";


            $fieldArray = array();
            foreach ($columns as $columnName => $type) {
                $fieldArray[] = "'".$columnName."'";
                $type = 'string';
                if (substr_count($type, 'int')) {
                    $type = 'int';
                }
                if (substr_count($type, 'float')) {
                    $type = 'float';
                }
                if (substr_count($type, 'decimal')) {
                    $type = 'float';
                }

                $methodName = ucfirst($columnName);
                $fdata .= "    /**\n";
                $fdata .= "     * Get {$columnName}\n";
                $fdata .= "     * @return $type\n";
                $fdata .= "     */\n";
                $fdata .= "    public function get{$methodName}() { return \$this->getField('{$columnName}');}\n";
                $fdata .= "\n";
                $fdata .= "    /**\n";
                $fdata .= "     * Set {$columnName}\n";
                $fdata .= "     * @param $type \${$columnName}\n";
                $fdata .= "     */\n";
                $fdata .= "    public function set{$methodName}($$columnName) {\$this->setField('$columnName', $$columnName);}\n";
                $fdata .= "\n";
            }

            $fdata .= "    /**\n";
            $fdata .= "     * Create an object\n";
            $fdata .= '     * @param int $id'."\n";
            $fdata .= "     */\n";
            $fdata .= '    public function __construct($id = 0) {'."\n";
            $fdata .= '        $this->setTablename(\''.$tableName.'\');'."\n";
            $fdata .= '        $this->setClassname(__CLASS__);'."\n";
            $fdata .= '        parent::__construct($id);'."\n";
            $fdata .= '    }'."\n";
            $fdata .= "\n";
            $fdata .= '    /**'."\n";
            $fdata .= '     * @return '.$this->_classesNameArray[$tableName]."\n";
            $fdata .= '     */'."\n";
            $fdata .= '    public function getNext($exception = false) {return parent::getNext($exception); }'."\n";
            $fdata .= "\n";
            $fdata .= '    /**'."\n";
            $fdata .= '     * @return '.$this->_classesNameArray[$tableName]."\n";
            $fdata .= '     */'."\n";
            $fdata .= '    public static function Get($key) {return self::GetObject("'.$this->_classesNameArray[$tableName].'", $key);}'."\n";
            $fdata .= "\n";
            $fdata .= "}\n";

            $fdata .= "\n";
            $fdata .= 'DBObject::SetFieldArray(\''.$tableName.'\', array('.implode(', ', $fieldArray).'));'."\n";


            // записываем класс в файл
            file_put_contents(
                Engine::Get()->getProjectPath().'/_api/db/'.$this->_classesNameArray[$tableName].'.class.php',
                $fdata,
                LOCK_EX
            );
        }
    }

    public static function Get() {
        if (!self::$_Instance) {
            self::$_Instance = new self();
        }
        return self::$_Instance;
    }

    private static $_Instance = null;
}