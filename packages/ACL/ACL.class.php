<?php

/**
 * Class ACL
 */
class ACL {

    public function __construct() {
        // var_dump($user = Engine_Auth::Get()->getUser()->getId());
    }

    private $aclJson = array();

    public function makeACLArray () {
        $aclArray = array();
        if ($this->getACLjsonArray()) {
            $i = 1;
            foreach ($this->getACLjsonArray() as $item) {
                // Получаем Section name
                if (isset($item->title) && $item->title) {
                    $aclArray[$item->section]['name'] = $item->title;
                } else {
                    $content = Engine_Content_Driver::Get()->getContentById($item->section.'');
                    if ($content) {
                        $aclArray[$item->section]['name'] = $content['title'];
                    }
                }

                if (!isset($item->name) || !$item->name) {
                    $aclArray[$item->section]['items'] = array();
                } else {
                    $code = $item->prefix.''.'_'.$item->code.'';

                    $group = false;
                    $name = $item->name;
                    if (isset($item->group) && $item->group) {
                        $group = $name = $item->group;

                        if (!isset($aclArray[$item->section]['items'][$group ? $group : $i]['group'])) {
                            // $aclArray[$item->section]['items'][$group ? $group : $i]['group'][] = array();
                        }
                        $aclArray[$item->section]['items'][$group ? $group : $i]['group'][] = array('name' => $item->name.'', 'code' => $code);

                    } else {
                        $aclArray[$item->section]['items'][$group ? $group : $i] = array('code' => $code, 'name' => $name, 'group' => false);
                    }


                }
                $aclArray[$item->section]['default_check'] = isset($item->default_check) ? (array)$item->default_check : array();

                $i++;
            }
        }
//        print '<pre>';
//        print_r($aclArray);
//        exit();
        return $aclArray;
    }

    public function getACLjsonArray () {
        if (!$this->aclJson) {
            $acl = file_get_contents(Engine::Get()->getProjectPath().'/_api/config/acl.json');
            if ($acl) {
                $this->aclJson = json_decode($acl);
            }
        }

        return $this->aclJson;
    }

//    private $userAclArray = array();
//    public function getUserAclArray ($userId = false) {
//        try {
//            if (!$userId) {
//                $user = Engine_Auth::Get()->getUser();
//                if ($user) {
//                    $userId = $user->getId();
//                }
//            }
//
//            if (!isset($this->userAclArray[$userId]) || !$this->userAclArray[$userId]) {
//                $this->userAclArray[$userId] = array();
//                if (class_exists('XUsersAcl')) {
//                    $userAcl = new XUserAcl();
//                    $userAcl->setUser_id($userId);
//                    while ($x = $userAcl->getNext()) {
//                        $this->userAclArray[$userId][] = $x->getCode();
//                    }
//                }
//
//            }
//            return $this->userAclArray[$userId];
//        } catch (Exception $e) {
//
//        }
//        return array();
//    }

    public function is ($code) {
        if (in_array($code, $this->getUserAclArray())) {
            return true;
        }
        return false;
    }

    public static function Get() {
        if (!self::$_Instance) {
            self::$_Instance = new self();
        }
        return self::$_Instance;

    }

    private static $_Instance = null;

}