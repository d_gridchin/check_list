<?php

/**
 * Class Engine_HTMLHead
 */
class Engine_HTMLHead {

    private $_sort = array();
    private $_index = 1;

    // Данная штука будет применятся только для контентов
    public function setSort ($sort) {
        array_unshift($this->_sort, $sort);
    }


    private $_filesArray = array();
    public function registerCssFile ($filePath , $sort = 'all') {
        $this->registerFile('css', $filePath, $sort);
    }

    public function registerJsFile ($filePath, $sort = 'all') {
        $this->registerFile('js', $filePath, $sort);
    }

    private function registerFile ($type, $filePath, $sort) {
        if ($sort === false) {
            $sort = 'end_of_list';
        }
        $filePath = str_replace('//', '/', $filePath);
        $this->_filesArray[$type][$sort][$this->_index] = $filePath;
        $this->_index++;
    }

    /**
     * @param $fileType
     * @return mixed
     */
    public function getFiles ($fileType) {
        return $this->_filesArray[$fileType];
    }

    private $_title = '';
    public function setTitle ($title) {
        $this->_title = $title;
    }

    public function makeHtmlHead () {
        if (Engine::Get()->getMode('dev')) {
            $v = ['', time(), ''];
        } else {
            $v = explode("|", file_get_contents(Engine::Get()->getProjectPath().'/.version'));
        }
        $this->setSort('all');
        $this->_sort[] = 'end_of_list';
        $jsFilesArray = array();
        $cssFilesArray = array();

        foreach ($this->_sort as $sortName) {
            if (isset($this->_filesArray['js'])) {
                if (isset($this->_filesArray['js'][$sortName])) {
                    foreach ($this->_filesArray['js'][$sortName] as $item) {
                        $jsFilesArray[] = $item.'?v='.$v[1];
                    }
                }

            }

            if (isset($this->_filesArray['css'])) {
                if (isset($this->_filesArray['css'][$sortName])) {
                    foreach ($this->_filesArray['css'][$sortName] as $item) {
                        $cssFilesArray[] = $item.'?v='.$v[1];
                    }
                }

            }
        }
        $t = new Template();
        $t->setTemplatePath(dirname(__FILE__).'/Engine_HTMLHead.phtml');
        $t->set('title', $this->_title);
        $t->set('cssFiles', $cssFilesArray);
        $t->set('jsFiles', $jsFilesArray);
        return $t->render();
    }


    /**
     * @return Engine_HTMLHead
     */
    public static function Get() {
        if (!self::$_Instance) {
            self::$_Instance = new self();
        }
        return self::$_Instance;
    }

    private static $_Instance = null;
}