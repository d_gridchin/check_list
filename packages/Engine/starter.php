<?php

$mem_start = memory_get_usage();

$time = microtime();
$time = explode(' ', $time);
$time = $time[1] + $time[0];
$start = $time;


session_start();
// Отключаем показ ошибок PHP (Ворнинги и нотисы)
ini_set('error_reporting', 0);
ini_set('display_errors', 0);
ini_set('display_startup_errors', 0);

// Путь к сессиям
// ini_set('session.save_path', $_SERVER['DOCUMENT_ROOT'] .'/media/session/');

include_once (dirname(__FILE__).'/include.php');
Engine::Get()->init();
Engine::Get()->execute();

if (Engine::Get()->getMode('report_time')) {
    $time = microtime();
    $time = explode(' ', $time);
    $time = $time[1] + $time[0];
    $finish = $time;
    $total_time = round(($finish - $start), 4);

    if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        // AJAX
    } else {
        $oz = round((memory_get_usage() / 1024 / 1024), 4).' MiB';
        echo '<div class="alert alert-info" style="position: fixed; bottom: -5px; right: 10px; display: inline-block; opacity: 0.6">Page generated in '.$total_time.' seconds.<br>OZY: '.$oz.'</div>';
    }
}