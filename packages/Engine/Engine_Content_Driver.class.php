<?php

/**
 * Class Engine_Content_Driver
 */
class Engine_Content_Driver {

    private $_baseFolder = '';
    private $_contents = array();
    public $_extendClass = 'Engine_Content';

    public function setBaseFolder ($folderPath) {
        $this->_baseFolder = $folderPath;
    }

    public function getBaseFolder () {
        return $this->_baseFolder;
    }

    public function registerContent ($contentId, $fieldsArray = array()) {
        $fieldsArray['id'] = $contentId;

        $folder = $this->_baseFolder;
        if (isset($fieldsArray['admin']) && $fieldsArray['admin']) {
            $folder = str_replace('//', '/', $this->_baseFolder.'/admin/');
        }

        if (isset($fieldsArray['php']) && $fieldsArray['php']) {
            $fieldsArray['php'] = $folder.'/'.$fieldsArray['php'];
            $php = $fieldsArray['php'];
        }

        if (isset($fieldsArray['html']) && $fieldsArray['html']) {
            $fieldsArray['html'] = $folder.'/'.$fieldsArray['html'];
            $html = $fieldsArray['html'];
        }

        // Все CSS файлы
        if (isset($fieldsArray['css']) && $fieldsArray['css']) {
            $b = $folder;
            if ($fieldsArray['css'][0] == 'real') {
                unset($fieldsArray['css'][0]);
                $b = '';
            }
            foreach ($fieldsArray['css'] as $path) {
                Engine_HTMLHead::Get()->registerCssFile($b.'/'.$path, $contentId);
            }
        }

        // Все JS файлы
        if (isset($fieldsArray['js']) && $fieldsArray['js']) {
            if (!is_array($fieldsArray['js'])) {
                $fieldsArray['js'] = (array) $fieldsArray['js'];
            }
            $b = $folder;;
            if ($fieldsArray['js'][0] == 'real') {
                unset($fieldsArray['js'][0]);
                $b = '';
            }
            foreach ($fieldsArray['js'] as $path) {
                if (!file_exists(Engine::Get()->getProjectPath().'/'.$b.'/'.$path)) {
                    file_put_contents(Engine::Get()->getProjectPath().'/'.$b.'/'.$path, '');
                }
                Engine_HTMLHead::Get()->registerJsFile($b.'/'.$path, $contentId);
            }
        }


        $this->_contents[$contentId] = $fieldsArray;

        if (Engine::Get()->getMode('development')) {
            if (@$php) {
                $dir = str_replace('//', '/', Engine::Get()->getProjectPath().'/'.substr($php, 0, strrpos($php, '/') + 1));
                if (!is_dir($dir)) {
                    mkdir($dir, 0755, true);
                }
                $file = str_replace('//', '//', Engine::Get()->getProjectPath().'/'.$php);
                if (!file_exists($file)) {
                    $classData = '<?php';
                    $classData .= "\n\n";
                    $classData .= "class ".basename($php, '.php')." extends ".$this->_extendClass." {";
                    $classData .= "\n\n";
                    $classData .= "    public function __construct() {\n\n    }\n\n";
                    $classData .= "    public function process() {\n\n    }\n\n}";
                    file_put_contents($file, $classData);
                }
            }

            if (@$html) {
                $dir = str_replace('//', '/', Engine::Get()->getProjectPath().'/'.substr($html, 0, strrpos($html, '/') + 1));
                if (!is_dir($dir)) {
                    mkdir($dir, 0755, true);
                }
                $file = str_replace('//', '//', Engine::Get()->getProjectPath().'/'.$html);
                if (!file_exists($file)) {
                    file_put_contents($file, '');
                }
            }

        }
    }

    /**
     * @param bool $url
     * @return bool
     */
    public function getContent ($url = false) {
        if ($url === false) {
            $url = @reset(explode('?', $_SERVER["REQUEST_URI"]));
        }

        foreach ($this->_contents as $contentId => $content) {
            $uri = @$content['url'];
            if (!$url) {
                continue;
            }

            if (!is_array($uri)) {
                $uri = (array) $uri;
            }

            foreach ($uri as $u) {
                if ($u == $url) {
                    return $this->_contents[$contentId];
                }
                

                $urlArray = explode('/', $url);
                $uArray = explode('/', $u);

                if (count($urlArray) == count($uArray)) {
                    $found = true;
                    $params = array();
                    foreach ($uArray as $k => $v) {
                        if ((strpos($v, '{') !== false) && (strpos($v, '}') !== false)) {
                            $params[str_replace('{', '', str_replace('}', '', $v))] = $urlArray[$k];
                            continue;
                        }
                        if ($urlArray[$k] != $uArray[$k]) {
                            $found = false;
                            break;
                        }
                    }
                    if ($found) {
                        // Нашли то что нада!!!
                        $this->_contents[$contentId]['params'] = $params;
                        return $this->_contents[$contentId];
                    }

                }
            }
        }
        return false;
    }

    /**
     * @param $contentId
     * @return bool
     */
    public function getContentById ($contentId) {
        if (isset($this->_contents[$contentId])) {
            return $this->_contents[$contentId];
        }
        return false;
    }

    /**
     * @return Engine_Content_Driver|null
     */
    public static function Get() {
        if (!self::$_Instance) {
            self::$_Instance = new self();
        }
        return self::$_Instance;
    }

    private static $_Instance = null;

}