<?php

/**
 * Class Engine
 */
class Engine {

    /* --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- */
    public function getProjectPath () {
        return dirname(dirname(dirname(__FILE__)));
    }

    public function errorReporting () {
        ini_set('error_reporting', E_ALL);
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
    }

    /* --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- */
    private $_modeArray = array();
    public function setMode ($mode) {
        $this->_modeArray[] = $mode;
    }

    public function getMode ($mode) {
        return in_array($mode, $this->_modeArray);
    }

    /* --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- */
    public function __construct() {

    }

    public function init () {
        PackageLoader::Get()->loadPackage('DB');
        include_once ($this->getProjectPath().'/config.php');
        include_once ($this->getProjectPath().'/_api/include.php');

    }

    public function getContentId () {
        return $this->_currentContentId;
    }


    public $_currentContentId = false;
    private $to = false;
    public function execute ($contentId = false) {
        try {
            if ($contentId) {
                $content = Engine_Content_Driver::Get()->getContentById($contentId);
            } else {
                $content = Engine_Content_Driver::Get()->getContent();
                if (!$content) {
                    $content = Engine_Content_Driver::Get()->getContentById('404');
                }
            }


            // Устанавливаем переменную
            $this->_currentContentId = $content['id'];

            // Генерируем событие
            Observer::Get()->observe('beforeEngineRender');

            // RENDER
            while (true) {
                if (isset($content['php']) && $content['php']) {
                    include_once $this->getProjectPath()."/".$content['php'].'';

                    if (isset($content['params']) && $content['params']) {
                        foreach ($content['params'] as $k => $v) {
                            Engine_URLParser::Get()->setArgument($k, $v);
                        }
                    }

                    $class = basename($content['php'], '.php');

                    // правильная сортировка JS / CSS Файлов
                    if (isset($content['title']) && $content['title']) {
                        Engine_HTMLHead::Get()->setTitle($content['title']);
                    }
                    Engine_HTMLHead::Get()->setSort($content['id']);

                    $x = new $class;
                    if (method_exists($x, 'process')) {
                        if ($r = $x->process()) {
                            return $r;
                        }
                    }
                }

                if (isset($content['html']) && $content['html']) {
                    $html = $content['html'];
                    Template::Get()->setTemplatePath($html);
                }
                if (isset($content['parent_content']) && $content['parent_content']) {
                    if (isset($content['html']) && $content['html']) {
                        $this->to = 'content';
                        if (isset($content['to']) && $content['to']) {
                            $this->to = $content['to'];
                        }

                        $c = Template::Get()->render();
                        Template::Get()->set($this->to, $c);
                    }

                    $content = Engine_Content_Driver::Get()->getContentById($content['parent_content']);
                } else {
                    if (isset($content['html']) && $content['html']) {
                        Template::Get()->display();
                    }
                    break;
                }
            }
            Observer::Get()->observe('afterEngineRender');
        } catch (Exception $e) {
            if ($this->getMode('log') && strpos($e->getMessage(), 'engine-content-content-') === false) {
                Engine_Log::Get()->log($e->getMessage());
            }

            if (strpos($e->getMessage(), 'engine-content-content-') !== false) {
                $this->execute(str_replace('engine-content-content-', '', $e->getMessage()));
            }
        }
        return true;
    }

    private $_configFieldsArray = array();
    public function setConfigField ($field, $value) {
        $this->_configFieldsArray[$field] = $value;
    }

    public function getConfigField ($field) {
        if (isset($this->_configFieldsArray[$field])) {
            return $this->_configFieldsArray[$field];
        }
        return false;
    }


    /* --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- */
    public static function Get() {
        if (!self::$_Instance) {
            self::$_Instance = new self();
        }
        return self::$_Instance;
    }

    private static $_Instance = null;
}