<?php

class Engine_URL_Maker {

    public function makeUrlSmart ($key, $value) {
        $arguments = Engine_Content::Get()->getArguments();
        $arguments[$key] = $value;

        $i = 0;
        $url = '';
        foreach ($arguments as $k => $p) {
            $paramString = $k.'='.$p;
            if ($i < 1) {
                $url .= "?".$paramString;
            } else {
                $url .= "&".$paramString;
            }
            $i++;
        }

        return $url;
    }

    public function makeUrlByContentIdGet ($contentId, $params = array(), $getArray = array()) {
        $url = $this->makeUrlByContentId($contentId, $params);

        $get = Engine_Content::Get()->getArguments();

        foreach ($params as $k => $v) {
            if (isset($get[$k])) {
                unset($get[$k]);
            }
        }

        foreach ($getArray as $k => $v) {
            $get[$k] = $v;
        }
        $getString = array();
        foreach ($get as $k => $v) {
            $getString[] = $k.'='.$v;
        }
        $url = $url.'?'.implode("&", $getString);
        return $url;
    }

    public function makeUrlByContentId ($contentId, $params = array()) {
        $content = Engine_Content_Driver::Get()->getContentById($contentId);
        if ($content) {
            if ($content['url']) {
                $url = $content['url'];
                if (is_array($url)) {
                    foreach ($url as $uri) {
                        // Разбиваем на массив
                        if (!$params && strpos($uri, '{') === false && strpos($uri, '}')) {
                            return $uri;
                        }
                        $urlItems = explode('/', $uri);

                        $paramsCount = 0;
                        foreach ($urlItems as $urlItem) {
                            if (strpos($urlItem, '{') !== false && strpos($urlItem, '}') !== false) {
                                $paramsCount++;
                            }
                        }


                        if ($paramsCount == count($params)) {
                            $returnUrl = $uri;
                            foreach ($params as $p => $v) {
                                $returnUrl = str_replace('{'.$p.'}', $v, $returnUrl);
                            }

                            return $returnUrl;
                        }
                    }
                } else {
                    // Нужно ли заменять какие-то параметры?
                    if (strpos($url, '{') !== false) {
                        foreach ($params as $k => $v) {
                            $url = str_replace('{'.$k.'}', $v, $url);
                        }
                    }
                }
                return $url;
            }
        }
        return '#';
    }

    public static function Get() {
        if (!self::$_Instance) {
            self::$_Instance = new self();
        }
        return self::$_Instance;
    }

    private static $_Instance = null;

}