<?php

/**
 * Class Engine_Content
 */
class Engine_Content {

    public function __construct() {

    }

    public function isDoctor () {
        return Engine::Get()->getConfigField('doctors');
    }

    public function setContentNotFound() {
        $this->setContentId('404');
    }

    public function setContentId ($contentId) {
        if (Engine::Get()->_currentContentId != $contentId) {
            if (!Engine::Get()->_currentContentId) {
                Engine::Get()->_currentContentId = $contentId;
            }
            throw new Exception('engine-content-content-'.$contentId);
        }
    }

    public function setValue($key, $value) {
        try {
            Template::Get()->set($key, $value);
        } catch (Exception $e) {
            print $e;
        }
    }

    public function setControlValue ($key, $value) {
        $this->setValue('control_'.$key, $value);
    }

    public function getValue ($key) {
        if (Template::Get()->__get($key)) {
            return Template::Get()->__get($key);
        }
        return false;
    }

    /**
     * @return array
     */
    public function getArguments() {
        return Engine_URLParser::Get()->getArguments();
    }

    public function getContentId () {
        return Engine::Get()->getContentId();
    }

    /**
     * @param $key
     * @return bool
     */
    public function getArgument($key) {
        return Engine_URLParser::Get()->getArgument($key);
    }

    public function getUser () {
        if (!Engine_Auth::Get()->getUser()) {
            throw new Exception('user-exist');
        }
        return Engine_Auth::Get()->getUser();
    }

    /**
     * @return bool|XUsers
     */
    public function getUserSecure () {
        try {
            return $this->getUser();
        } catch (Exception $e) {
            return false;
        }
    }

    public function isAllowAcl ($code) {
        return ACL_Service::Get()->allow($code);
    }

    public function isDenyAcl ($code) {
        return ACL_Service::Get()->deny($code);
    }





    /**
     * @return Engine_Content
     */
    public static function Get() {
        if (!self::$_Instance) {
                self::$_Instance = new self();
        }
        return self::$_Instance;
    }

    private static $_Instance = null;
}