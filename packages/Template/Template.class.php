<?php

class Template {

    public function __construct() {
        if (class_exists('ACL_Service')) {
            Observer::Get()->observe('beforeEngineRender');
        }
    }

    private $_data = array();
    public function set($key, $value) {
        $this->_data[$key] = $value;
    }

    // Переменные которые будут передаватся во все контеты
    private $_data2All = array();
    public function set2All ($key, $value) {
        $this->_data2All[$key] = $value;
    }

    /* Метод для удаления значений из данных для вывода */
    public function delete($name) {
        if (isset ($this->_data[$name])) {
            unset($this->_data[$name]);
        } elseif (isset($this->_data2All[$name])) {
            unset($this->_data2All[$name]);
        }
    }

    public function clearData () {
        $this->_data = array();
    }

    public function __get($name) {
        if (isset($this->_data[$name])) {
            return $this->_data[$name];
        } elseif (isset($this->_data2All[$name])) {
            return $this->_data2All[$name];
        }
        return "";
    }

    private $_templatePath = '';
    public function setTemplatePath ($path) {
        $this->_templatePath = $path;
    }

    public function render () {
        ob_start();
        $filePath = Engine::Get()->getProjectPath().str_replace(Engine::Get()->getProjectPath(), '', $this->_templatePath);
        include ("{$filePath}");
        $c = ob_get_clean();

        $this->clearData();

        return $c;
    }

    public function display() {
        echo $this->render();
    }

    public static function Get() {
        if (!self::$_Instance) {
            self::$_Instance = new self();
        }
        return self::$_Instance;
    }

    private static $_Instance = null;
}