var loading = loading || (function ($) {
    'use strict';
    var loading_window = $('<div/>').css({'display': 'none', 'position': 'fixed', 'top': '0', 'left': '0', 'width': '100vw', 'height':'100vh', 'background': 'rgba(0, 0, 0, 0.8)', 'z-index': '200500'}).append($('<img/>').attr('src', '/_images/loading.gif').css({'width': '70px', 'position': 'absolute', 'top': '50%', 'left': '50%', 'margin-left': '-35px', 'margin-top': '-35px'}));
    $('html').append(loading_window);

    return {
        show: function () {
            loading_window.fadeIn(150);
        },
        hide: function () {
            loading_window.fadeOut(50);
        }
    };

})(jQuery);