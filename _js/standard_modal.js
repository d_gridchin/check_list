// Модальное окно
var standard_modal = standard_modal || (function ($) {
        'use strict';

        var dialog = $('<div/>').addClass("modal fade").append($('<div/>').addClass('modal-dialog').append($('<div/>').addClass('modal-content').append($('<div/>').addClass('modal-header').append($('<h4/>').addClass('modal-title'))).append($('<div/>').addClass('main-content'))));
        $('body').append(dialog);

        dialog.on('hidden.bs.modal', function () {
            dialog.find('.modal-dialog').removeClass('modal-lg');
            dialog.find('.modal-dialog').removeClass('modal-sm');
        })

        return {
            getModal: function () {
                return dialog;
            },
            setSize: function (size) {
                if (size == 'lg') {
                    dialog.find('.modal-dialog').addClass('modal-lg');
                }
                if (size == 'sm') {
                    dialog.find('.modal-dialog').addClass('modal-sm');
                }
                if (size == 'xl') {
                    dialog.find('.modal-dialog').css('width', '90%');
                }


                return standard_modal;

            },
            clearTitle: function () {
                dialog.find('.js-modal-title').html('');
                return standard_modal;
            },
            setTitle: function (title) {
                dialog.find('.modal-title').html(title);
                return standard_modal;
            },
            setContent: function (content) {
                dialog.find('.main-content').html(content);
                return standard_modal;
            },
            show: function (stat) {
                if (stat) {
                    dialog.attr('data-backdrop', 'static').attr('data-keyboard', 'false');
                }
                // console.log(dialog.find('.modal-title').html());
                if (!dialog.find('.js-modal-title').html()) {
                    dialog.find('.modal-header').hide();
                } else {
                    dialog.find('.modal-header').show();
                }
                dialog.modal('show');
                return standard_modal;
            },
            hide: function () {
                dialog.modal('hide');
            },
            shake: function () {
                dialog.find('.modal-content').addClass('shake');
                setTimeout(function () {
                    dialog.find('.modal-content').removeClass('shake');
                }, 300)
            }
        };
    })(jQuery);