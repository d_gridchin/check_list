$(function () {
    $('input[type="tel"]').mask('0999999999');
    $('[data-toggle="tooltip"]').tooltip();

    var section = false;
    $('.js-upload-photo').on('click', function (e) {
        var button = false;
        e.preventDefault();
        $('input[name="upload_once_file"]').click();

        section = $(this).data('section');

        $('input[name="section"]').val(section);
    });

    $('input[name="upload_once_file"]').on('change', function (e) {
        loading.show();
        var input = $(this);
        e.preventDefault();

        var formData = new FormData(input.closest('form')[0]);
        console.log(formData);

        $.ajax({
            url: "/upload_once_image/",
            type: "POST",
            data: formData,
            dataType: 'json',
            async: false,
            success: function (data) {

                loading.hide();
                var title = "<a href='javascript: void(0);' class='js-comment-image' data-id='"+data.id+"'><i class='fa fa-comment'></i> Комментарии</a> :: <a href='javascript: void(0);'  class='js-delete-image' data-id='\"+data.id+\"'><i class='fa fa-trash'></i> Удалить</a>";
                var image = $('<a/>').attr({'href': data.path, 'rel': 'group', 'class': 'js-box', 'title': title}).append($('<img/>').attr({src: data.path, 'data-id': data.id}).addClass('once-image'));


                if (section == 'question') {
                    $('.js-image-row').append(image);
                }

                if (section == 'blank') {
                    image.attr('rel', 'group-all');
                    $('.js-image-row-blank').append(image);
                    setTimeout(function () {
                        $('.once-image[data-id="'+data.id+'"]').click();
                    }, 300);
                }

            },
            cache: false,
            contentType: false,
            processData: false
        });
    });



    $(document).ready(function() {
        function createCommentBlock(id) {
            loading.show();
            var commentForm = $('<form/>').attr('method', 'post')
                .append(
                    $('<input/>').attr({'type': 'hidden', 'name': 'photo_id', 'value': id})
                )
                .append(
                    $('<textarea/>').addClass('form-control').attr({'row': '3', 'name': 'text'})
                )

                .append(
                    $('<input/>').addClass('btn btn-block btn-default btn-sm').css('margin-top', '2px').attr({'value': 'Добавить комментарий', 'name': 'ok', 'type': 'submit'})
                )
                .on('submit', function (e) {
                    loading.show();
                    e.preventDefault();

                    $.post('/admin/blank-photo-add-comment', commentForm.serializeArray(), function (data) {
                        $('.js-modal-comments-block').html(createCommentBlock(id));
                    })
                })

            var commentsItems = $('<div/>').css({'max-height': '80vh', 'overflow': 'auto'}).append($('<div/>').addClass('alert alert-info').text('Комментариев пока нет'));

            var commentBlock = $('<div/>')
                .append(
                    commentsItems
                )
                .append(
                    commentForm
                )

            console.log(commentBlock);

            $.post('/admin/blank-photo-get-comment', {id: id}, function (data) {
                setTimeout(function () {
                    loading.hide();
                }, 300);

                if (data.length) {
                    commentsItems.html('');

                    $.each($(data), function (i, v) {
                        var onceComment = $('<div/>').addClass('alert alert-info').css('margin-bottom', '2px')
                            .append(
                                $('<div/>')
                                    .append(
                                        $('<small/>').css({'float': 'left', 'display': 'block'}).text(v.user_name)
                                    )
                                    .append(
                                        $('<small/>').css({'float': 'right', 'display': 'block'}).text(v.date)
                                    )
                                    .append($('<div/>').css('clear', 'both'))
                            )
                            .append(
                                $('<div/>').css('color', '#333').text(v.text)
                            )
                        commentsItems.append(onceComment)
                    })
                }


            }, 'json');

            return commentBlock;




        }
        $('body').on('click', '.js-box', function (e) {
            e.preventDefault();
            var element = $(this);

            var rel = element.attr('rel');
            var id = element.find('img').data('id');
            var href = element.attr('href');


            var currentIndex = 0;
            var groupArray = [];

            console.log(groupArray);



            // Нужно сформировать весь список этой группы
            $.each($('body .js-box[rel="'+rel+'"]'), function (i, v) {
                var index = i+1;
                var element = $(v).clone();
                element.attr('title', '');

                if (!element.hasClass('js-control-carousel')) {
                    groupArray[index] = element;
                    if (element.find('img').data('id') == id) {
                        currentIndex = index;
                    }
                }
            });

            var nextUrl = false;
            var prevUrl = false;

            if (currentIndex >= 1) {
                nextUrl = groupArray[currentIndex+1];
            }
            if (currentIndex < groupArray.length && currentIndex > 1) {
                prevUrl = groupArray[currentIndex-1];
            }




            var controlButton = false;
            if (nextUrl || prevUrl) {
                controlButton = $('<div/>').css({
                    'position': 'absolute',
                    'top': '15px',
                    'left': '30px',
                    'width': 'calc(100% - 60px)'
                });
                if (nextUrl) {
                    nextUrl.css({'float': 'right'}).addClass('btn btn-sm btn-link js-control-carousel');
                    nextUrl.append($('<i/>').addClass('fa fa-angle-right fa-4x'));
                    nextUrl.find('img').hide();
                    controlButton.append(
                        nextUrl
                    )
                }
                if (prevUrl) {
                    prevUrl.css({'float': 'left'}).addClass('btn btn-sm btn-link js-control-carousel');
                    prevUrl.append($('<i/>').addClass('fa fa-angle-left fa-4x'));
                    prevUrl.find('img').hide();


                    controlButton.append(
                        prevUrl
                    );
                }
            }





            var content = $('<div/>').addClass('modal-body');
            content.append(
                $('<div/>').addClass('row')
                    .append(
                        $('<div/>').addClass('col-lg-8 col-md-8 col-sm-12 col-xs-12')
                            .append(controlButton)
                            .append(
                                $('<img/>').attr({'src': href}).css({'display': 'block', 'margin': '0 auto', 'max-width': '100%', 'max-height': '90vh', 'box-shadow': '0 0 20px #999'})
                            )
                    )
                    .append(
                        $('<div/>').addClass('col-lg-4 col-md-4 col-sm-12 col-xs-12 js-modal-comments-block')
                            .append(
                                createCommentBlock(id)
                            )
                    )

            )

            standard_modal.setContent(content).setSize('xl').show();


        })
        // $(".js-box").fancybox({
        //     'titlePosition' : 'inside'
        // });

    });

    $('body').on('click', '.js-comment-image', function (e) {
        loading.show();
        e.preventDefault();
        var element = $(this);
        var id = $(this).data('id');


        $.fancybox.close();
        setTimeout(function () {
            loading.hide();

            $.post('/photo-comment', {id: id, type: 'get_comment'}, function (comment) {
                var comment_form = $('<form/>')
                    .append($('<strong/>').text('Вы можете оставить комментарий'))
                    .append($('<input/>').attr({'name': 'type', 'type': 'hidden', 'value': 'update'}))
                    .append($('<input/>').attr({'name': 'id', 'type': 'hidden', 'value': id}))
                    .append($('<textarea/>').addClass('form-control').attr('name', 'comment').val(comment).css('margin-bottom', '3px'))
                    .append($('<input/>').addClass('btn btn-sm btn-default').attr({'name': 'ok', 'type': 'submit', 'value': 'Оставить комментарий'}));

                comment_form.on('submit', function (e) {
                    e.preventDefault();

                    loading.show();
                    $.post('/photo-comment', comment_form.serializeArray(), function (comment) {
                        alert('Домментарий сохранен');
                        loading.hide();
                    });
                });

                standard_modal.setTitle('Комментарии').setContent (
                    $('<div>')
                        .append(
                            $('<div/>').addClass('modal-body')
                                .append(comment_form)
                        )
                ).show();
            });
        }, 500);
    })

    $('body').on('click', '.js-delete-image', function (e) {
        e.preventDefault();
        var element = $(this);
        var id = $(this).data('id');

        loading.show();
        $.post('/delete-once-image', {id: id}, function () {
            document.location.reload();
            // element.hide();
            //
            // setTimeout(function () {
            //     loading.hide();
            // }, 300);
        })
    })


    $('.js-control-question').on('click', function (e) {
        e.preventDefault();
        loading.show();

        var id = $(this).data('id');
        var form = $('<form/>');


        $.post('/admin/question-control', {id: id, type: 'get_info'}, function (data) {
            var category_select = create_form_item.create_item({name: 'category_id', title: 'Категория', element: 'select', value: data.category_array});
            var category_input = create_form_item.create_item({name: 'category', title: 'Добавление категории. Введите название категории   '}).css('display', 'none');
            var name_input = create_form_item.create_item({name: 'name', title: 'Название'});
            var rate_input = create_form_item.create_item({name: 'rate', title: 'Максимальный бал, или оценка за да'});

            if (!data.show_category) {
                category_select = $('<span/>');
            }

            if (data.status == 'ok' && data.rate) {
                rate_input.find('input').val(data.rate);
            }

            category_select.find('select').on('change', function (e) {
               if ($(this).val() == 'add-new') {
                   category_select.hide();
                   category_input.show();
               }
            });

            var type = $('<div/>').addClass('btn-group').data('toggle', 'buttons');
            $.each(data.type_array, function (i, v) {
                var label = $('<label/>').addClass('btn btn-default btn-sm');
                var radio = $('<input/>').attr({'type': 'radio', 'name': 'system_type', 'id': i, 'value': i,  'autocomplete': 'off'}).css('display', 'none');

                if (data.type == i) {
                    label.addClass('active');
                    radio.attr('checked', 'checked');
                }


                label
                    .append(radio)
                    .append(v)

                type.append(label);
            });

            type.find('input[type="radio"]').on('change', function () {
                type.find('.btn.active').removeClass('active');
                var changed = $(this);
                changed.closest('.btn').addClass('active');
            });


            if (data.status == 'ok' && data.name) {
                name_input.find('input').val(data.name);
            }

            var deleteQuestionButton = $('<a/>').addClass('btn btn-sm btn-danger').html('Удалить').attr('href', '#');

            deleteQuestionButton.on('click', function (e) {
                e.preventDefault();
                if (confirm('Вы уверены что хотите удалить это вопросы')) {
                    $.post('/admin/question-control', {id: id, type: 'delete'}, function (data) {
                        document.location.reload();
                    });
                }

            });

            form
                .append(
                    $('<div/>').addClass('modal-body')
                        .append(
                            $('<div/>').addClass('row')
                                .append($('<div/>').addClass('col-lg-12 col-md-12 col-sm-12 col-xs-12').append(category_select))
                                .append($('<div/>').addClass('col-lg-12 col-md-12 col-sm-12 col-xs-12').append(category_input))
                                .append($('<div/>').addClass('col-lg-12 col-md-12 col-sm-12 col-xs-12').append(name_input))
                                .append($('<div/>').addClass('col-lg-12 col-md-12 col-sm-12 col-xs-12').append(
                                    $('<div/>')
                                        .append($('<strong/>').html('Тип ответа<br>'))
                                        .append(type)
                                ))
                                .append($('<div/>').addClass('col-lg-12 col-md-12 col-sm-12 col-xs-12').append(rate_input))

                        )
                )
                .append(
                    $('<div/>').addClass('modal-footer')
                        .append(deleteQuestionButton)
                        .append($('<input/>').attr({'type': 'submit', 'name': 'ok', 'value': 'Save'}).addClass('btn btn-sm btn-default'))
                );

            loading.hide();
            standard_modal.setTitle('Вопрос').setContent(form).show();
        }, 'json');

        form.on('submit', function (e) {
            e.preventDefault();

            var formData = new FormData($(this)[0]);

            $.ajax({
                url: "/admin/question-control?id="+id+"&type=control",
                type: "POST",
                dataType: 'json',
                data: formData,
                async: false,
                success: function (data) {
                    if (data.status == 'ok') {
                        document.location.reload();
                    }
                },
                cache: false,
                contentType: false,
                processData: false
            });

            e.preventDefault();

            // $.post('/admin/catalog-control', {id: id, type: 'control', form: form.serializeArray()}, function (data) {

            // }, 'json')
        });
    });

    $('.js-delete-category').on('click', function (e) {
        e.preventDefault();
        var categoryId = $(this).data('category_id');
        if (confirm('Вы уверены что хотите удалить категорию, и все вопросы в этой категории?')) {
            $.post('/admin/question-control', {id: categoryId, type: 'delete_category'}, function (data) {
                document.location.reload();
            });
        }
    });


    $('.js-control-shops').on('click', function (e) {
        e.preventDefault();
        loading.show();

        var id = $(this).data('id');
        var form = $('<form/>');


        $.post('/admin/shop-control', {id: id, type: 'get_info'}, function (data) {
            var number_input = create_form_item.create_item({name: 'number', title: 'Номер магазина'});
            var address_input = create_form_item.create_item({name: 'address', element: 'text', title: 'Адрес'});
            var city_input = create_form_item.create_item({name: 'city', title: 'Город'});

            if (data.status == 'ok' && data.number) {
                number_input.find('input').val(data.number);
            }
            if (data.status == 'ok' && data.city) {
                city_input.find('input').val(data.city);
            }

            if (data.status == 'ok' && data.address) {
                address_input.find('textarea').val(data.address);
            }

            var modal_content = $('<div/>').addClass('row');

            modal_content
                .append($('<div/>').addClass('col-lg-12 col-md-12 col-sm-12 col-xs-12').append(number_input))
                .append($('<div/>').addClass('col-lg-12 col-md-12 col-sm-12 col-xs-12').append(city_input))
                .append($('<div/>').addClass('col-lg-12 col-md-12 col-sm-12 col-xs-12').append(address_input))

            var $input = city_input.find('input');
            $input.typeahead({
                source: function (query, process) {
                    return $.post('/admin/shops/search-city', {name: query}, function (data) {
                        var d = $.parseJSON(data);
                        return process(d);
                    });
                }
            });


            if (data.type_list == 'auditor') {
                var fop_input = create_form_item.create_item({name: 'fop', title: 'ФОП'});
                var tm_select = create_form_item.create_item({name: 'tm_id', element: 'select', title: 'Териториальный менеджер', value: data.tm_array});
                var rm_select = create_form_item.create_item({name: 'rm_id', element: 'select', title: 'Региональный менеджер', value: data.rm_array});

                var $input = fop_input.find('input');
                $input.typeahead({
                    source: function (query, process) {
                        return $.post('/admin/shops/search-fop', {name: query}, function (data) {
                            var d = $.parseJSON(data);
                            return process(d);
                        });
                    }
                });

                if (data.status == 'ok' && data.fop) {
                    fop_input.find('input').val(data.fop);
                }

                modal_content
                    .append($('<div/>').addClass('col-lg-12 col-md-12 col-sm-12 col-xs-12').append(fop_input))
                    .append($('<div/>').addClass('col-lg-12 col-md-12 col-sm-12 col-xs-12').append(tm_select))
                    .append($('<div/>').addClass('col-lg-12 col-md-12 col-sm-12 col-xs-12').append(rm_select))
            }

            if (data.type_list == 'inventory') {
                var region_input = create_form_item.create_item({name: 'region', title: 'Регион'});
                var president_select = create_form_item.create_item({name: 'president_id', element: 'select', title: 'Глава инв. комисии', value: data.president_array});

                var $input = region_input.find('input');
                $input.typeahead({
                    source: function (query, process) {
                        return $.post('/admin/shops/search-region', {name: query}, function (data) {
                            var d = $.parseJSON(data);
                            return process(d);
                        });
                    }
                });

                if (data.status == 'ok' && data.region) {
                    region_input.find('input').val(data.region);
                }

                modal_content
                    .append($('<div/>').addClass('col-lg-12 col-md-12 col-sm-12 col-xs-12').append(region_input))
                    .append($('<div/>').addClass('col-lg-12 col-md-12 col-sm-12 col-xs-12').append(president_select))
            }







            var deleteButton = $('<a/>').addClass('btn btn-sm btn-danger').html($('<i/>').addClass('fa fa-trash')).attr('href', '#');
            deleteButton.on('click', function (e) {
                e.preventDefault()
                $.post('/admin/shop-control', {id: id, type: 'delete'}, function (data) {
                    document.location.reload();
                });
            });




            form
                .append(
                    $('<div/>').addClass('modal-body')
                        .append(
                            modal_content
                        )
                )
                .append(
                    $('<div/>').addClass('modal-footer')
                        .append(deleteButton)
                        .append($('<input/>').attr({'type': 'submit', 'name': 'ok', 'value': 'Save'}).addClass('btn btn-sm btn-default'))
                );

            loading.hide();
            standard_modal.setTitle('Вопрос').setContent(form).show();
        }, 'json');

        form.on('submit', function (e) {
            e.preventDefault();

            form.append($('<input/>').attr({'type': 'hidden', 'name': 'id', 'value': id}));
            form.append($('<input/>').attr({'type': 'hidden', 'name': 'type', 'value': 'control'}));
            $.post('/admin/shop-control', form.serializeArray(), function (data) {
                document.location.reload();
            }, 'json')
        });
    });









    $('.js-create-new-blank').on('click', function (e) {
        e.preventDefault();



        var form = $('<form/>');
        var name_input = create_form_item.create_item({name: 'name', title: 'Название магазина'});
        form
            .append(
                $('<div/>').addClass('modal-body')
                    .append(
                        $('<div/>').addClass('row')
                            .append($('<div/>').addClass('col-lg-12 col-md-12 col-sm-12 col-xs-12').append(name_input))

                    )
            )
            .append(
                $('<div/>').addClass('modal-footer').append($('<input/>').attr({'type': 'submit', 'name': 'ok', 'value': 'Save'}).addClass('btn btn-sm btn-default'))
            );

        standard_modal.setTitle('Новый бланк').setContent(form).show();

        form.on('submit', function (e) {
            e.preventDefault()
            if (name_input.find('input').val()) {
                loading.show();
                e.preventDefault();

                $.post('/admin/create-new-blank', form.serializeArray(), function (data) {
                    standard_modal.setContent(
                        $('<div/>').addClass('modal-body')
                            .append($('<strong/>').html('По данной ссылке доступен этот бланк.<br>'))
                            .append($('<small/>').html(data.url))
                    );
                    loading.hide();
                }, 'json');
            } else {
                alert('Укажите название магазина');
            }



        });
    })
});