var create_form_item = create_form_item || (function ($) {
        'use strict';

        return {
            create_item: function (settings) {
                var defaults = {
                    element         : 'input',
                    name            : '',
                    value           : '',
                    title           : '',
                    text_row_cnt    : 3,
                    mask            : false
                };

                var settings = $.extend(defaults, settings);

                var element = $('<div/>').addClass('form-group')

                if (settings.type == 'hidden') {
                    element.css('display', 'none');
                }
                if (settings.title && settings.element != 'checkbox') {
                    element.append($('<label/>').attr('for', settings.name).text(settings.title));
                }

                var item = $('<div/>').addClass('input-group').css('width', '100%');

                if (settings.element == 'checkbox' && settings.title) {
                    item.append($('<label/>').css('font-weight', 'normal').append($('<input/>').attr({'type': 'checkbox', 'value': '1', 'name': settings.name})).append('&nbsp;&nbsp;'+settings.title));
                }
                if (settings.element == 'input') {
                    var input = $('<input/>').addClass('form-control').attr({'id': settings.id, 'type': settings.type, 'name': settings.name, 'value': settings.value});
                    if (settings.mask) {
                        input.mask(settings.mask);
                    }
                    item.append(input);
                }
                if (settings.element == 'text') {
                    item.append($('<textarea/>').addClass('form-control').attr({'id': settings.id, 'name': settings.name, 'value': settings.value, 'rows': settings.text_row_cnt}))
                }


                if (settings.element == 'select') {
                    var select = $('<select/>').addClass('form-control').attr({'id': settings.id, 'name': defaults.name});

                    if (settings.value) {
                        $.each($(settings.value), function (index, select_option) {
                            var option = $('<option/>').text(select_option.name).attr('value', select_option.value);
                            if (select_option.selected) {
                                option.attr('selected', 'selected');
                            }
                            select.append(option);
                        });
                    }

                    item.append(select);
                }


                element.append(item);

                return element;
            }
        };

    })(jQuery);
