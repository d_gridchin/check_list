<?php

$dir = str_replace(Engine::Get()->getProjectPath(), '', dirname(__FILE__));
Engine_HTMLHead::Get()->registerJsFile($dir.'/js/bootstrap-select.js', 'end_of_list');
Engine_HTMLHead::Get()->registerCssFile($dir.'/css/bootstrap-select.min.css', 'end_of_list');