<?php

$dir = str_replace(Engine::Get()->getProjectPath(), '', dirname(__FILE__));

Engine_HTMLHead::Get()->registerCssFile($dir.'/bootstrap-editable.css', 'end_of_list');
Engine_HTMLHead::Get()->registerCssFile($dir.'/datetimepeacker.min.css', 'end_of_list');
Engine_HTMLHead::Get()->registerJsFile($dir.'/bootstrap-editable.min.js', 'end_of_list');
Engine_HTMLHead::Get()->registerJsFile($dir.'/moment.js', 'end_of_list');
Engine_HTMLHead::Get()->registerJsFile($dir.'/combodate.js', 'end_of_list');
Engine_HTMLHead::Get()->registerJsFile($dir.'/address.js', 'end_of_list');
Engine_HTMLHead::Get()->registerJsFile($dir.'/price_comment.js', 'end_of_list');
Engine_HTMLHead::Get()->registerJsFile($dir.'/datetimepicker.min.js', 'end_of_list');