(function ($) {
    "use strict";

    var price_comment = function (options) {
        this.init('price_comment', options, price_comment.defaults);
    };

    //inherit from Abstract input
    $.fn.editableutils.inherit(price_comment, $.fn.editabletypes.abstractinput);

    $.extend(price_comment.prototype, {
        /**
         Renders input from tpl

         @method render()
         **/
        render: function() {
            this.$input = this.$tpl.find('input');
            this.$comment = this.$tpl.find('textarea');
        },

        /**
         Default method to show value in element. Can be overwritten by display option.

         @method value2html(value, element)
         **/
        value2html: function(value, element) {
            if(!value) {
                $(element).empty();
                return;
            }
            var html = $('<div>').text(value.price).html() + ', ' + $('<div>').text(value.comment).html();
            $(element).html(html);
        },

        /**
         Converts value to string.
         It is used in internal comparing (not for sending to server).

         @method value2str(value)
         **/
        value2str: function(value) {
            var str = '';
            if(value) {
                for(var k in value) {
                    str = str + k + ':' + value[k] + ';';
                }
            }
            return str;
        },

        /*
         Converts string to value. Used for reading value from 'data-value' attribute.

         @method str2value(str)
         */
        str2value: function(str) {
            /*
             this is mainly for parsing value defined in data-value attribute.
             If you will always set value by javascript, no need to overwrite it
             */
            return str;
        },

        /**
         Sets value of input.

         @method value2input(value)
         @param {mixed} value
         **/
        value2input: function(value) {
            if(!value) {
                return;
            }
            this.$input.filter('[name="price"]').val(value.price);
            this.$comment.filter('[name="comment"]').val(value.comment);
            // this.$input.filter('[name="building"]').val(value.building);
        },

        /**
         Returns value of input.

         @method input2value()
         **/
        input2value: function() {
            return {
                price: this.$input.filter('[name="price"]').val(),
                comment: this.$comment.filter('[name="comment"]').val(),
                // building: this.$input.filter('[name="building"]').val()
            };
        },

        /**
         Activates input: sets focus on the first field.

         @method activate()
         **/
        activate: function() {
            this.$input.filter('[name="price"]').focus();
        },

        /**
         Attaches handler to submit form in case of 'showbuttons=false' mode

         @method autosubmit()
         **/
        autosubmit: function() {
            this.$input.keydown(function (e) {
                if (e.which === 13) {
                    $(this).closest('form').submit();
                }
            });
        }
    });

    price_comment.defaults = $.extend({}, $.fn.editabletypes.abstractinput.defaults, {
        tpl: '<div class="editable-address"><label><span>Price: </span><input type="text" name="price" class="form-control"></label></div>'+
        '<div class="editable-address"><label><span>Reason: </span><textarea name="comment" class="form-control"></textarea></label></div>',

        inputclass: ''
    });

    $.fn.editabletypes.price_comment = price_comment;

}(window.jQuery));