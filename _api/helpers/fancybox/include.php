<?php

$dir = str_replace(Engine::Get()->getProjectPath(), '', dirname(__FILE__));
Engine_HTMLHead::Get()->registerJsFile($dir.'/jquery.fancybox-1.3.4.js', 'end_of_list');
Engine_HTMLHead::Get()->registerJsFile($dir.'/jquery.easing-1.3.pack.js', 'end_of_list');
Engine_HTMLHead::Get()->registerJsFile($dir.'/jquery.mousewheel-3.0.4.pack.js', 'end_of_list');

Engine_HTMLHead::Get()->registerCssFile($dir.'/jquery.fancybox-1.3.4.css', 'end_of_list');