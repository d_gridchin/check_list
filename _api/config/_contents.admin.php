<?php

Engine_Content_Driver::Get()->registerContent('tpl-admin', array (
    'php'               => '/template/tpl_admin.php',
    'html'              => '/template/tpl_admin.phtml',
    'parent_content'    => 'html-template',
    'admin'             => true
));


// Главная страница
Engine_Content_Driver::Get()->registerContent('admin-index', array (
    'title'             => 'Отчеты',
    'url'               => array('/admin/'),
    'php'               => '/index/index.php',
    'html'              => '/index/index.phtml',
    'parent_content'    => 'tpl-admin',
    'admin'             => true
));

Engine_Content_Driver::Get()->registerContent('admin-index-ajax-load', array (
    'url'               => array('/admin/ajax-load'),
    'php'               => '/index/index_ajax_load.php',
    'html'              => '/index/index_ajax_load.phtml',
    'admin'             => true
));

// Управление пользователями
Engine_Content_Driver::Get()->registerContent('admin-users', array (
    'title'             => 'Пользователи',
    'url'               => array('/admin/users', '/admin/users/{id}'),
    'php'               => '/users/users_index.php',
    'html'              => '/users/users_index.phtml',
    'parent_content'    => 'tpl-admin',
    'admin'             => true
));

// Все вопросы
Engine_Content_Driver::Get()->registerContent('admin-questions', array (
    'title'             => 'Все вопросы',
    'url'               => array('/admin/questions'),
    'php'               => '/questions/index.php',
    'html'              => '/questions/index.phtml',
    'parent_content'    => 'tpl-admin',
    'admin'             => true
));
Engine_Content_Driver::Get()->registerContent('admin-question-control', array (
    'url'               => array('/admin/question-control'),
    'php'               => '/questions/control.php',
    'admin'             => true
));

// Бланки
Engine_Content_Driver::Get()->registerContent('admin-blanks', array (
    'title'             => 'Бланки',
    'url'               => array('/admin/blanks'),
    'php'               => '/blanks/index.php',
    'html'              => '/blanks/index.phtml',
    'parent_content'    => 'tpl-admin',
    'admin'             => true
));
// Бланки
Engine_Content_Driver::Get()->registerContent('admin-blanks-ajax-load-data', array (
    'url'               => array('/admin/blanks-ajax-load-data'),
    'php'               => '/blanks/index_ajax_load_data.php',
    'html'              => '/blanks/index_ajax_load_data.phtml',
    'admin'             => true
));
// Просмотр бланка
Engine_Content_Driver::Get()->registerContent('admin-blank-view', array (
    'url'               => array('/admin/blank/{id}'),
    'php'               => '/blanks/view.php',
    'html'              => '/blanks/view.phtml',
    'parent_content'    => 'tpl-admin',
    'admin'             => true
));

// Получить комментарии по фотограции
Engine_Content_Driver::Get()->registerContent('admin-blank-photo-get-comments', array (
    'url'               => array('/admin/blank-photo-get-comment'),
    'php'               => '/blanks/get_photo_comments.php',
    'admin'             => true
));

// Добавить комментарий к фотограци
Engine_Content_Driver::Get()->registerContent('admin-blank-photo-фвв-comments', array (
    'url'               => array('/admin/blank-photo-add-comment'),
    'php'               => '/blanks/add_photo_comments.php',
    'admin'             => true
));



// Магазины
Engine_Content_Driver::Get()->registerContent('admin-shops', array (
    'title'             => 'Магазины',
    'url'               => array('/admin/shops'),
    'php'               => '/shops/shops_index.php',
    'html'              => '/shops/shops_index.phtml',
    'parent_content'    => 'tpl-admin',
    'admin'             => true
));
Engine_Content_Driver::Get()->registerContent('admin-shop-control', array (
    'url'               => array('/admin/shop-control'),
    'php'               => '/shops/shops_control.php',
    'admin'             => true
));
Engine_Content_Driver::Get()->registerContent('admin-shop-search-city', array (
    'url'               => array('/admin/shops/search-city'),
    'php'               => '/shops/search_city.php',
    'admin'             => true
));
Engine_Content_Driver::Get()->registerContent('admin-shop-search-fop', array (
    'url'               => array('/admin/shops/search-fop'),
    'php'               => '/shops/search_fop.php',
    'admin'             => true
));

Engine_Content_Driver::Get()->registerContent('admin-shop-search-region', array (
    'url'               => array('/admin/shops/search-region'),
    'php'               => '/shops/search_region.php',
    'admin'             => true
));

// logout
Engine_Content_Driver::Get()->registerContent('logout', array (
    'url'               => '/logout',
    'php'              => '/admin/auth/logout.php',
));


// SMART REPORT
if (Engine::Get()->getConfigField('check_list_type') == 'inventory') {
    // Главная страница
    Engine_Content_Driver::Get()->registerContent('admin-smart-report', array (
        'title'             => 'Smart Report',
        'url'               => array('/reports/smart'),
        'php'               => '/reports/index_smart.php',
        'html'              => '/reports/index_smart.phtml',
        'parent_content'    => 'tpl-admin',
        'admin'             => true
    ));

    Engine_Content_Driver::Get()->registerContent('admin-smart-report-ajax-load', array (
        'url'               => array('/reports/smart-ajax-load'),
        'php'               => '/reports/index_smart_ajax_load.php',
        'html'              => '/reports/index_smart_ajax_load.phtml',
        'admin'             => true
    ));
}
