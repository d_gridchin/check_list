<?php

if (Engine::Get()->getMode('development')) {
    // Это системуные таблицы, которые удалять нельзя.
    // В случаи удаления, что-то может пойти не так, и движек не стартонет.

    $prefix = Engine::Get()->getConfigField('db_prefix');


    foreach (Engine::Get()->getConfigField('listArray') as $prefix) {
        SyncProcessor::Get()->syncTableFields($prefix.'_users', array (
            array('name', 'varchar(255)'),
            array('login', 'varchar(100)'),
            array('password', 'varchar(32)'),
            array('last_active', 'int(11)'),
            array('role', 'enum("'.implode('", "', array_keys(Auth_Service::Get()->getAllRoleArray())).'")'),
            array('hash', 'varchar(32)'),
            array('deleted', 'tinyint(1)'),
        ), 'X'.$prefix.'Users');

        $questionTypeArray = Questions_Service::Get()->getTypeArray();
        SyncProcessor::Get()->syncTableFields($prefix.'_questions', array (
            array('category_id', 'int(11)'),
            array('name', 'varchar(255)'),
            array('type', 'enum("'.implode('", "', array_keys($questionTypeArray)).'")'),
            array('rate', 'tinyint(1)'),
            array('deleted', 'tinyint(1)'),
        ), 'X'.$prefix.'Questions');

        SyncProcessor::Get()->syncTableFields($prefix.'_blank', array (
            array('shop_id', 'int(11)'),
            array('add_date', 'date'),
            array('complete_date', 'datetime'),
            array('auditor_id', 'int(11)'),
            array('additional_info', 'text'),

            array('customRate', 'tinyint(1)'),
            array('rate', 'int(11)'),
        ), 'X'.$prefix.'Blank');

        SyncProcessor::Get()->syncTableFields($prefix.'_blank_question', array (
            array('blank_id', 'int(11)'),
            array('question_id', 'int(11)'),

            array('rate', 'int(11)'),
            array('individual', 'tinyint(1)'),

            array('note', 'text'),
            array('comment', 'text'),

            array('progress_auditor', 'tinyint(1)'),
            array('progress_comment', 'tinyint(1)'),
            array('auditor_complete_date', 'datetime'),
        ), 'X'.$prefix.'BlankQuestion');

        SyncProcessor::Get()->syncTableFields($prefix.'_images', array (
            array('path', 'varchar(255)'),
            array('comment', 'text'),
            array('type', 'enum("question", "blank")'),
            array('parent_id', 'int(11)'),
        ), 'X'.$prefix.'Images');


        SyncProcessor::Get()->syncTableFields($prefix.'_image_comments', array (
            array('image_id', 'int(11)'),
            array('comment', 'text'),
            array('datetime', 'datetime'),
            array('user_ud', 'int(11)'),
        ), 'X'.$prefix.'ImageComments');



    }

    SyncProcessor::Get()->syncTableFields('_question_category', array (
        array('name', 'varchar(255)'),
        array('deleted', 'tinyint(1)'),
    ), 'XQuestionCategory');

    SyncProcessor::Get()->syncTableFields('_city', array (
        array('name', 'varchar(255)'),
    ), 'XCity');

    SyncProcessor::Get()->syncTableFields('_fop', array (
        array('name', 'varchar(255)'),
    ), 'XFop');

    SyncProcessor::Get()->syncTableFields('_region', array (
        array('name', 'varchar(255)'),
    ), 'XRegion');

    SyncProcessor::Get()->syncTableFields('_shops', array (
        array('number', 'int(4)'),
        array('address', 'varchar(255)'),
        array('city_id', 'int(11)'),

        array('fop_id', 'int(11)'),
        array('tm_user_id', 'int(11)'),
        array('rm_user_id', 'int(11)'),

        array('region_id', 'int(11)'),
        array('president_id', 'int(11)'),

        array('deleted', 'tinyint(1)'),
    ), 'XShops');


    SyncProcessor::Get()->syncTableFieldsProcess();

}