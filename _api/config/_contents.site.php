<?php

Engine_Content_Driver::Get()->registerContent('html-template', array (
    'php'               => '/template/html_template.php',
    'html'              => '/template/html_template.phtml',
));



Engine_Content_Driver::Get()->registerContent('site-template', array (
    'php'               => '/site/template/site_template.php',
    'html'              => '/site/template/site_template.phtml',
    'parent_content'    => 'html-template'
));


// 403 (Авторизация в админ-панель)
Engine_Content_Driver::Get()->registerContent('admin_auth', array (
    'url'               => '/auth',
    'title'             => 'Авторизация',
    'php'               => '/admin/auth/auth.php',
    'html'              => '/admin/auth/auth.phtml',
    'parent_content'    => 'html-template'
));

Engine_Content_Driver::Get()->registerContent('auditor-auth', array (
    'url'               => '/auditor-auth',
    'title'             => 'Авторизация',
    'php'               => '/site/auth/auth.php',
    'html'              => '/site/auth/auth.phtml',
    'parent_content'    => 'html-template'
));

// Главная страница
Engine_Content_Driver::Get()->registerContent('index', array (
    'title'             => Engine::Get()->getConfigField('title'),
    'url'               => array('/'),
    'php'               => '/site/index/index.php',
    'html'              => '/site/index/index.phtml',
    'parent_content'    => 'site-template'
));

// --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- Мы зарегистрировались. Нужно показать логин и пароль для входа
Engine_Content_Driver::Get()->registerContent('auditor-confirm-registration', array (
    'title'             => Engine::Get()->getConfigField('title'),
    'url'               => array('/blank/registration-confirm/{hash}'),
    'php'               => '/site/registration_confirm.php',
    'html'              => '/site/registration_confirm.phtml',
    'parent_content'    => 'site-template'
));

Engine_Content_Driver::Get()->registerContent('blank-confirm-start', array (
    'title'             => Engine::Get()->getConfigField('title'),
    'url'               => array('/blank/confirm-start/{blank_hash}'),
    'php'               => '/site/blank/confirm_start.php',
    'html'              => '/site/blank/confirm_start.phtml',
    'parent_content'    => 'site-template'
));

Engine_Content_Driver::Get()->registerContent('blank-auditor', array (
    'title'             => Engine::Get()->getConfigField('title'),
    'url'               => array('/blank/{hash}', '/blank/{hash}/{question_number}'),
    'php'               => '/site/blank/blank_auditor_index.php',
    'html'              => '/site/blank/blank_auditor_index.phtml',
    'parent_content'    => 'site-template'
));
Engine_Content_Driver::Get()->registerContent('blank-for-tm', array (
    'title'             => Engine::Get()->getConfigField('title'),
    'url'               => array('/blank-tm/{hash}', '/blank-tm/{hash}/{question_number}'),
    'php'               => '/site/blank/index.php',
    'html'              => '/site/blank/index.phtml',
    'parent_content'    => 'site-template'
));

// --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- Анкета заполнена
Engine_Content_Driver::Get()->registerContent('blank-all-questions-complete', array (
    'title'             => Engine::Get()->getConfigField('title'),
    'url'               => array('/blank-all-questions-complete'),
    'html'              => '/site/blank/all_questions_complete.phtml',
    'parent_content'    => 'site-template'
));

// --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- Отправить фото на сервер
Engine_Content_Driver::Get()->registerContent('upload-once-image', array (
    'url'               => array('/upload_once_image/'),
    'php'               => '/upload_once_image.php',
));

Engine_Content_Driver::Get()->registerContent('delete-once-image', array (
    'url'               => array('/delete-once-image'),
    'php'               => '/delete_once_image.php',
));

Engine_Content_Driver::Get()->registerContent('once-image-comment', array (
    'url'               => array('/photo-comment'),
    'php'               => '/once_image_comment.php',
));