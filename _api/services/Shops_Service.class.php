<?php

class Shops_Service {

    private $_shopArray = [];
    public function getList ($showDeleted = false) {
        $shops = new XShops();
        $shops->setOrder('number');

        // Получаем фильтры
        $cityId = Engine_Content::Get()->getArgument('filter_city_id');
        $rmId = Engine_Content::Get()->getArgument('filter_rm_id');
        $tmId = Engine_Content::Get()->getArgument('filter_tm_id');
        $fopId = Engine_Content::Get()->getArgument('filter_fop_id');
        $filter_president = Engine_Content::Get()->getArgument('filter_president');
        $filter_region = Engine_Content::Get()->getArgument('filter_region');

        // Накладываем фильтры
        if ($cityId) {
            $shops->setCity_id($cityId);
        }
        if ($rmId) {
            $shops->setRm_user_id($rmId);
        }
        if ($tmId) {
            $shops->setTm_user_id($tmId);
        }
        if ($fopId) {
            $shops->setFop_id($fopId);
        }
        if ($filter_president) {
            $shops->setPresident_id($filter_president);
        }
        if ($filter_region) {
            $shops->setRegion_id($filter_region);
        }






        if (!$showDeleted) {
            $shops->setDeleted(0);
        }
        while ($x = $shops->getNext()) {
            $this->_shopArray[$x->getId()] = $this->getShopItem($x);
        }
        return $this->_shopArray;
    }

    private function getShopItem (XShops $x) {
        return [
            'id' => $x->getId(),
            'city' => $this->getCityById($x->getCity_id()),
            'city_id' => $x->getCity_id(),

            'fop' => $this->getFopById($x->getFop_id()),
            'fop_id' => $x->getFop_id(),

            'region' => $this->getRegionById($x->getRegion_id()),
            'region_id' => $x->getRegion_id(),

            'number' => $x->getNumber(),
            'address' => $x->getAddress(),

            'tm' => Auth_Service::Get()->getUserById($x->getTm_user_id()),
            'rm' => Auth_Service::Get()->getUserById($x->getRm_user_id()),
            'president' => Auth_Service::Get()->getUserById($x->getPresident_id()),

            'deleted' => $x->getDeleted()
        ];
    }

    public function getShopById ($id) {
        $shop = new XShops();
        $shop->setId($id);
        if ($shop->select()) {
            return $this->getShopItem($shop);
        }
        return false;
    }

    public function getCityById ($cityId) {
        if ($cityId) {
            $city = new XCity();
            $city->setId($cityId);
            if ($city->select()) {
                return $city->getName();
            }
        }

        return false;
    }

    public function getFopById ($fopId) {
        if ($fopId) {
            $fop = new XFop();
            $fop->setId($fopId);
            if ($fop->select()) {
                return $fop->getName();
            }
        }

        return false;
    }

    public function getRegionById ($regionId) {
        if ($regionId) {
            $region = new XRegion();
            $region->setId($regionId);
            if ($region->select()) {
                return $region->getName();
            }
        }

        return false;
    }

    public static function Get() {
        if (!self::$_Instance) {
            self::$_Instance = new self();
        }
        return self::$_Instance;
    }
    private static $_Instance = null;

}