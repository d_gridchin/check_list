<?php

class Date_Service {


    public function getRuDayName ($date) {
        $number = date('N', strtotime($date));

        $nameArray = array('', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье');
        return $nameArray[$number];
    }
    public function getRuMonthName ($date) {
        $number = date('n', strtotime($date));

        $nameArray = array('', 'Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь');
        return $nameArray[$number];
    }

    public function getRuMonthNameSmall ($date) {
        $number = date('n', strtotime($date));

        $nameArray = array('', 'январь', 'февраль', 'март', 'апрель', 'май', 'июнь', 'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь');
        return $nameArray[$number];
    }

    public function getDayArray () {
        return [
            '1' => 'Понедельник',
            '2' => 'Вторник',
            '3' => 'Среда',
            '4' => 'Четверг',
            '5' => 'Пятница',
            '6' => 'Суббота',
            '7' => 'Воскресенье',
        ];
    }

    public function getOldByBDay($bDay) {
        $birth = new DateTime($bDay);
        $today     = new DateTime();
        $interval  = $today->diff($birth);

        return ['y' => $interval->format('%y'), 'm' => $interval->format('%m')];
    }

    public function number($n, $titles) {
        $cases = array(2, 0, 1, 1, 1, 2);
        return $titles[($n % 100 > 4 && $n % 100 < 20) ? 2 : $cases[min($n % 10, 5)]];
    }


    public static function Get() {
        if (!self::$_Instance) {
            self::$_Instance = new self();
        }
        return self::$_Instance;
    }
    private static $_Instance = null;

}