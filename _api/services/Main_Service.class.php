<?php

class Main_Service {

    public function getProtocol ($slash = true) {
        $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https" : "http";
        if ($slash) {
            $protocol .= '://';
        }

        return $protocol;
    }

    public function getSiteAddress () {
        return Main_Service::Get()->getProtocol().str_replace('//', '/', $_SERVER['HTTP_HOST']);
    }

    public function addAdditionalInfoToBlank ($blank, $k, $v) {
        $a = $blank->getAdditional_info();
        if ($a) {
            $a = json_decode($a, true);
        } else {
            $a = [];
        }
        $a[$k] = $v;

        $blank->setAdditional_info(json_encode($a));
        $blank->update();
    }

    public static function Get() {
        if (!self::$_Instance) {
            self::$_Instance = new self();
        }
        return self::$_Instance;
    }
    private static $_Instance = null;

}