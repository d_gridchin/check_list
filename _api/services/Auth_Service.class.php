<?php

class Auth_Service {

    public function isLogin () {
        if ($this->getUser()) {
            return true;
        }

        return false;
    }

    public function getAllRoleArray () {
         return ['admin' => 'Администратор', 'manager' => 'Менеджер', 'auditor' => 'Ревизор', 'tm' => 'Териториальный менеджер', 'rm' => 'Региональный менеджер', 'viewer' => 'Просмотрищик', 'president' => 'Глава инвентаризационной комиссии'];
    }

    private $_roleArray = [];
    public function getRoleArray () {
        if (Engine::Get()->getConfigField('check_list_type') == 'auditor') {
            if (!$this->_roleArray) {
                $this->_roleArray = ['admin' => 'Администратор', 'manager' => 'Менеджер', 'auditor' => 'Ревизор', 'tm' => 'Териториальный менеджер', 'rm' => 'Региональный менеджер'];
            }
        }
        if (Engine::Get()->getConfigField('check_list_type') == 'inventory') {
            if (!$this->_roleArray) {
                $this->_roleArray = ['admin' => 'Администратор', 'viewer' => 'Просмотрищик', 'president' => 'Глава инвентаризационной комиссии'];
            }
        }



        return $this->_roleArray;
    }

    public function getRoleByKey ($key) {
        $roles = $this->getRoleArray();
        if (isset($roles[$key]) && $roles[$key]) {
            return ['key' => $key, 'name' => $roles[$key]];
        }

        return false;
    }

    private $_usersArray = [];
    public function getUserArray () {
        if (!$this->_usersArray) {
            $user = new Users();

            while ($x = $user->getNext()) {
                $this->_usersArray[$x->getId()] = [
                    'id' => $x->getId(),
                    'login' => $x->getLogin(),
                    'role' => $this->getRoleByKey($x->getRole()),
                    'role_key' => $x->getRole(),
                    'name' => $x->getName(),
                    'deleted' => $x->getDeleted(),
                ];
            }
        }
        return $this->_usersArray;
    }

    public function getUserById ($id) {
        $users = $this->getUserArray();
        if (isset($users[$id]) && $users[$id]) {
            return $users[$id];
        }
        return false;
    }

    public function addUser ($name, $login, $password) {
        $user = new Users();
        $user->setLogin($login);
        $user->setLogin($login);
        if (!$user->select()) {
            $user->setName($name);
            $user->setPassword($this->createPasswordHash($password));
            $user->insert();
        }
    }

    public function login ($login, $password) {
        if (class_exists('Users')) {
            $user = new Users();
            $user->setLogin($login);
            $user->setPassword($this->createPasswordHash($password));
            if ($user->select()) {
                $hash = $this->createUserHash($user->getLogin(), $user->getPassword());

                $user->setHash($hash);
                $user->update();

                $_SESSION['user_hash'] = $hash;

                return true;

            }
        }
        return false;
    }

    public function logOut () {
        if (isset($_SESSION['user_hash']) && $_SESSION['user_hash']) {
            $user = $this->getUser();
            if ($user) {
                $user->setHash('');
                $user->update();
            }
            $_SESSION['user_hash'] = false;
        }
    }

    public function getUser () {
        if (isset($_SESSION['user_hash']) && $_SESSION['user_hash']) {
            if (class_exists('Users')) {
                $user = new Users();
                $user->setHash($_SESSION['user_hash']);
                if ($user->select()) {
                    $user->setLast_active(time());
                    $user->update();
                    return $user;
                }
            }

            return false;
        }

        return false;
    }

    public function createPasswordHash ($password) {
        return md5($password.md5($password.$password));
    }
    public function createUserHash ($login, $password) {
        return md5($login.md5($password.$login));
    }


    public static function Get() {
        if (!self::$_Instance) {
            self::$_Instance = new self();
        }
        return self::$_Instance;
    }
    private static $_Instance = null;

}