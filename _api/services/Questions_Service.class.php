<?php

class Questions_Service {


    private $_questionsArrayNotDeleted = [];
    private $_questionsArrayDeleted = [];
    public function getAllQuestionsArray ($showDeleted = false) {
        $find = false;
        if ($showDeleted) {
            if (!$this->_questionsArrayDeleted) $find = true;
        } else {
            if (!$this->_questionsArrayNotDeleted) $find = true;
        }


        if ($find) {
            $questions = new Questions();
            if (!$showDeleted) {
                $questions->setDeleted(0);
            }
            while ($x = $questions->getNext()) {
                $a = [
                    'id' => $x->getId(),
                    'category_id' => $x->getCategory_id(),
                    'name' => $x->getName(),
                    'type' => $x->getType(),
                    'rate' => $x->getRate(),
                    'type_name' => $this->getTypeNameByType($x->getType()),
                    'deleted' => $x->getDeleted()
                ];

                if ($showDeleted) {
                    $this->_questionsArrayDeleted[$x->getId()] = $a;
                } else {
                    $this->_questionsArrayNotDeleted[$x->getId()] = $a;
                }
            }
        }


        if ($showDeleted) {
            return $this->_questionsArrayDeleted;
        } else {
            return $this->_questionsArrayNotDeleted;
        }
    }

    /**
     * @param $id
     * @return bool
     */
    public function getQuestionById ($id) {
        $questions = $this->getAllQuestionsArray(true);
        if (isset($questions[$id]) && $questions[$id]) {
            return $questions[$id];
        }

        return false;
    }

    public function getTypeNameByType ($type) {
        $types = $this->getTypeArray();
        if (isset($types[$type]) && $types[$type]) {
            return $types[$type];
        }
        return false;
    }

    public function getTypeArray () {
        return [
            'yes_no' => 'Да / Нет',
            'rate' => 'Оценка',
        ];
    }

    public function getCategoryArray ($showDeleted = false) {
        $questionCategory = new XQuestionCategory();
        if (!$showDeleted) {
            $questionCategory->setDeleted(0);
        }
        $questionCategoryArray = [];

        while ($x = $questionCategory->getNext()) {
            $questionCategoryArray[$x->getId()] = ['id' => $x->getId(), 'name' => $x->getName()];
        }
        return $questionCategoryArray;
    }

    public function getCategoryById ($id) {
        $categoryArray = $this->getCategoryArray();
        if (isset($categoryArray[$id]) && $categoryArray[$id]) {
            return $categoryArray[$id];
        }
        return false;
    }

    public function getReportBlankAll ($date) {
        $filterCityId = Engine_Content::Get()->getArgument('filter_city_id');
        $filterAuditorId = Engine_Content::Get()->getArgument('filter_auditor_id');
        $filterTmId = Engine_Content::Get()->getArgument('filter_tm_id');
        $filterRmId = Engine_Content::Get()->getArgument('filter_rm_id');

        $filterPresidentId = Engine_Content::Get()->getArgument('filter_president_id');
        $filterRegionId = Engine_Content::Get()->getArgument('filter_region_id');

        $filterAuditorComplete = Engine_Content::Get()->getArgument('filter_auditor_complete');
        $filterCompleteComments = Engine_Content::Get()->getArgument('filter_complete_comments');

        $filterChartRate = Engine_Content::Get()->getArgument('char_rate');
        if ($filterChartRate) {
            $filterChartRate = explode("-", $filterChartRate);
        }
        


        $blank = new Blank();
        $blank->addWhere('add_date', date('Y-m-', strtotime($date)).'01', '>=');
        $blank->addWhere('add_date', date('Y-m-t', strtotime($date)), '<=');

        $blankArray = [];
        while ($x = $blank->getNext()) {
            // Получаем данные о магазине
            $shop = Shops_Service::Get()->getShopById($x->getShop_id());
            if ($shop['deleted']) {
                continue;
            }

            $completeDate = $x->getComplete_date();

            $tmComplete = Questions_Service::Get()->checkBlankComplete($x->getId());

            $info = $this->getBlankInfo($x->getId(), $tmComplete);
            if ($x->getRate()) {
                $rate = $x->getRate();
            } else {
                if ($info['cnt'] && $info['rate']) {
                    $rate = $info['rate'] / $info['cnt'];
                } else {
                    $rate = 0;
                }
            }




            // Дата завершения опроса
            if ($completeDate == '0000-00-00 00:00:00') {
                $completeDate = '?';
            } else {
                $completeDate = date('d', strtotime($completeDate)).' '.Date_Service::Get()->getRuMonthNameSmall($completeDate).', '.date('Y', strtotime($completeDate)).'<br>'.date('H:i', strtotime($completeDate));
            }


            if (Engine::Get()->getConfigField('check_list_type') == 'auditor') {
                $rate = floor($rate);
            } else {
                $rate = number_format($rate, 2, '.', '');
            }

            $add = true;
            if ($shop && is_array($shop) && $filterCityId && $shop['city_id'] != $filterCityId) {
                $add = false;
            }
            if ($filterAuditorId && $x->getAuditor_id() != $filterAuditorId) {
                $add = false;
            }
            if ($shop && is_array($shop) && $filterRmId && $shop['rm']['id'] != $filterRmId) {
                $add = false;
            }
            if ($shop && is_array($shop) && $filterTmId && !in_array($shop['tm']['id'], $filterTmId)) {
                $add = false;
            }
            if ($shop && is_array($shop) && $filterPresidentId && $shop['president']['id'] != $filterPresidentId) {
                $add = false;
            }
            if ($shop && is_array($shop) && $filterPresidentId && $shop['region_id'] != $filterRegionId) {
                $add = false;
            }
            if ($filterAuditorComplete) {
                if ($filterAuditorComplete == 'yes' && !$info['complete']) {
                    $add = false;
                }
                if ($filterAuditorComplete == 'no' && $info['complete']) {
                    $add = false;
                }
            }
            if ($filterCompleteComments) {
                if ($filterCompleteComments == 'yes' && !$tmComplete) {
                    $add = false;
                }
                if ($filterCompleteComments == 'no' && $tmComplete) {
                    $add = false;
                }
            }
            if ($filterChartRate) {
                if (($rate < $filterChartRate[1] || $rate > $filterChartRate[0])) {
                    $add = false;
                } else {
//                    $add = false;
                }
            }



            if ($add) {

                $blankArray[] = [
                    'id' => $x->getId(),
                    'shop' => $shop,
                    'date' => $x->getAdd_date(),

                    'auditor' => Auth_Service::Get()->getUserById($x->getAuditor_id()),
                    'auditor_complete' => $info['complete'],

                    'rm_id' => $shop['rm']['id'],
                    'rm_name' => $shop['rm']['name'],
                    'tm_complete' => $tmComplete,
                    'compete_date' => $completeDate,

                    'rate' => $rate
                ];
            }

        }
        $rm_id = array();
        $rm_name = array();
        $shopNumber = array();
        foreach ($blankArray as $k => $v) {
            $rm_id[$k] = $v['rm_id'];
            $rm_name[$k] = $v['rm_name'];
            $shopNumber[$k] = $v['shop']['number'];
        }
        array_multisort($shopNumber, SORT_ASC, $blankArray);

        return $blankArray;
    }

    public function getBlankInfo ($blankId, $tmComplete) {
        $result = ['rate' => 0, 'cnt' => 0, 'complete' => true];
        $blankQuestion = new BlankQuestion();
        $blankQuestion->setBlank_id($blankId);
        while ($bq = $blankQuestion->getNext()) {
            if (!$bq->getProgress_auditor()) {
                $result['complete'] = false;
            }

            if ($tmComplete) {
                $q = Questions_Service::Get()->getQuestionById($bq->getQuestion_id());
                if ($q) {
                    if (!$bq->getIndividual()) {
                        $result['rate'] += (($bq->getRate() / $q['rate']) * 100);
                        $result['cnt']++;
                    }

                }

            }
        }
        return $result;
    }

    public function checkBlankComplete ($blankId) {
        $complete = true;

        $blankQuestion = new BlankQuestion();
        $blankQuestion->setBlank_id($blankId);
        while ($bq = $blankQuestion->getNext()) {

            if (!$bq->getProgress_auditor()) {
                $complete = false;
                break;
            }
        }

        return $complete;
    }

    public static function Get() {
        if (!self::$_Instance) {
            self::$_Instance = new self();
        }
        return self::$_Instance;
    }
    private static $_Instance = null;

}