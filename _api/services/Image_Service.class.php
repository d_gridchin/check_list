<?php

class Image_Service {

    public function makeUploadPath ($name, $tmpName) {
        return '/media/'.time().'.'.$this->getExtension($name);
    }

    public function getExtension ($filename) {
        return substr(strrchr($filename, '.'), 1);
    }
    


    public static function Get() {
        if (!self::$_Instance) {
            self::$_Instance = new self();
        }
        return self::$_Instance;
    }
    private static $_Instance = null;

}