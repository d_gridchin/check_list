<?php

// Верификация
if (!empty($_SERVER) && isset($_SERVER['HTTP_HOST'])) {
    $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || @$_SERVER['SERVER_PORT'] == 443) ? "https" : "http";
    define('verification_url', $protocol."://".$_SERVER['HTTP_HOST']."/");
}






// Загружаем нужные нам пакеты
PackageLoader::Get()->loadPackage('Template');
PackageLoader::Get()->loadPackage('ACL');
PackageLoader::Get()->loadPackage('jQuery');
PackageLoader::Get()->loadPackage('Bootstrap');
PackageLoader::Get()->loadPackage('Observer');
PackageLoader::Get()->loadPackage('PHPMailer');
PackageLoader::Get()->loadPackage('QR');
PackageLoader::Get()->loadPackage('SimpleImage');

// PackageLoader::Get()->loadHelper('fancybox');


// Проверяем, какой сейчас включен чек-лист?
if (!Engine::Get()->getConfigField('db_prefix') && !Engine::Get()->getConfigField('check_list_type')) {
    $listArray = ['auditor', 'inventory'];
    Engine::Get()->setConfigField('listArray', $listArray);

    $currentList = 'auditor';


    $host = $_SERVER['HTTP_HOST'];
    if (strpos($host, 'cla') !== false) {
        $currentList = 'auditor';
    }
    if (strpos($host, 'cli') !== false) {
        $currentList = 'inventory';
    }


    PackageLoader::Get()->loadPackage('SynchronizationProcessor');
    Template::Get()->set2All('check_list_type', $currentList);

    Engine::Get()->setConfigField('db_prefix', $currentList);
    Engine::Get()->setConfigField('check_list_type', $currentList);
}


// Говорим системме, что у нас есть такие системные репозитории:
PackageLoader::Get()->registerPhpDirectory('/_api/db/');
PackageLoader::Get()->registerPhpDirectory('/_api/services/');
PackageLoader::Get()->registerPhpDirectory('/_api/system/');


// Все базы данных, и минимальные настройки
include_once (dirname(__FILE__).'/config/_xdb.php');

// Все контенты
Engine_Content_Driver::Get()->setBaseFolder('/contents/');
include_once (dirname(__FILE__).'/config/_contents.admin.php');
include_once (dirname(__FILE__).'/config/_contents.site.php');



// Нужно добавить в базу inventory
if (Engine::Get()->getMode('development')) {

}

class checkAuth {
    public function notify () {
        $content = Engine_Content_Driver::Get()->getContentById(Engine::Get()->getContentId());
        if (!Auth_Service::Get()->isLogin() && isset($content['admin']) && $content['admin']) {
            Engine_Content::Get()->setContentId('admin_auth');
        }
    }
}

if (Engine::Get()->getMode('development')) {
    Auth_Service::Get()->addUser('Dima Gridchin (Developer)', 'dima', 'Dimaloll23');
}
Observer::Get()->addEvent('beforeEngineRender', new checkAuth);
