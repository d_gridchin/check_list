<?php

ini_set('memory_limit', '-1');


ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);


print "update version...\n";
$versionFile = dirname(__FILE__).'/.version';

$v = 1;
$time = time();
$date = date('YmdHis');

$data = file_get_contents($versionFile);
if ($data) {
    $data = explode("|", $data);
    $v = $data[0] + 1;
}
file_put_contents($versionFile, implode("|", array($v, $time, $date)));

print "update database...\n";
include_once dirname(__FILE__).'/packages/Engine/include.php';
Engine::Get()->setMode('development');
Engine::Get()->init();

print "\nDONE\n\n";
