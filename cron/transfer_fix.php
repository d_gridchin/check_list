<?php


//
include_once dirname(__FILE__).'/../packages/Engine/include.php';

if ($argv[1] == 'auditor' || $argv[1] == 'inventory') {
    Engine::Get()->setConfigField('check_list_type', $argv[1]);
    Engine::Get()->setConfigField('db_prefix', $argv[1]);
}


Engine::Get()->init();

$image = new Images();
while ($x = $image->getNext()) {
    $path = 'http://cla.standarts.avrora.ua'.str_replace("http://rg.drgreensoft.com", '', $x->getPath());


    $x->setPath($path);
    $x->update();

    if ($x->getComment()) {
        $imageComment = new ImageComments();
        $imageComment->setComment($x->getComment());
        $imageComment->setDatetime('2018-01-01 00:00:00');
        $imageComment->setUser_ud(0);
        $imageComment->setImage_id($x->getId());
        $imageComment->insert();
    }
}