<?php

include_once dirname(__FILE__).'/../packages/Engine/include.php';

if ($argv[1] == 'auditor' || $argv[1] == 'inventory') {
    Engine::Get()->setConfigField('check_list_type', $argv[1]);
    Engine::Get()->setConfigField('db_prefix', $argv[1]);
}


Engine::Get()->init();

$shop = new XShops();
$shop->setDeleted(0);
while ($x = $shop->getNext()) {
    print "Shop #".$x->getId()."\n";
    $blank = new Blank();
    $blank->setShop_id($x->getId());
    $blank->addWhere('add_date', date('Y-m-').'01', '>=');
    $blank->addWhere('add_date', date('Y-m-t'), '<=');

    if (!$blank->getNext()) {
        print "Add new blank. \n";
        $blank->setAdd_date(date('Y-m-d'));
        $blank->insert();

        // Получаем все вопросы
        $questions = new Questions();
        $questions->setDeleted(0);
        while ($q = $questions->getNext()) {
            $bq = new BlankQuestion();
            $bq->setBlank_id($blank->getId());
            $bq->setQuestion_id($q->getId());
            if (!$bq->select()) {
                $bq->insert();
            }
        }

    }
}

print "\nDONE\n\n";